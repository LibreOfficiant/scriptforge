The **SFWidgets** folder is a subfolder of the [LibreOffice core repository tree](https://opengrok.libreoffice.org/xref/core/).

It contains the Basic source code pertaining to the SFWidgets library.

Destination: [xref: /core/wizards/source/sfwidgets/](https://opengrok.libreoffice.org/xref/core/wizards/source/sfwidgets/)
