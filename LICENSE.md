Copyright 2019-2025 Jean-Pierre LEDURE, Rafael LIMA, @AmourSpirit, Alain ROMEDENNE


**ScriptForge** is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

**ScriptForge** is free software; you can redistribute it and/or modify it under the terms of either (at your option):

1) The Mozilla Public License, v. 2.0. If a copy of the MPL was not
	distributed with this file, you can obtain one at [http://mozilla.org/MPL/2.0/](URL) .

2) The GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. If a copy of the LGPL was not distributed with this file, see [http://www.gnu.org/licenses/](URL) .

========================================================================

All text and image content in the current directory and subdirectories is licensed under the Creative Commons Attribution-Share Alike 4.0 License (<u>unless otherwise specified</u>).

*“LibreOffice”* and *“The Document Foundation”* are registered trademarks. Their respective logos and icons are subject to international copyright laws. The use of these thereof is subject to trademark policy.


