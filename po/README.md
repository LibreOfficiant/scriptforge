The **po** folder is a subfolder of the [LibreOffice core repository tree](https://opengrok.libreoffice.org/xref/core/).

It contains the reference and its translations of all the english strings used in the ScriptForge software.

Destination: [xref: /core/wizards/source/scriptforge/po/](https://opengrok.libreoffice.org/xref/core/wizards/source/scriptforge/po/)
