The **Build** tree is a small subset of the [LibreOffice core repository tree](https://opengrok.libreoffice.org/xref/core/).

It lists the source files (essentially make files) needed to have the ScriptForge piece of software integrated nicely in a LibreOffice release.

The effective source code is elsewhere.
