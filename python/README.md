The **python** folder is a subfolder of the [LibreOffice core repository tree](https://opengrok.libreoffice.org/xref/core/).

It contains the Python source code being part of the ScriptForge software.

Destination: [xref: /core/wizards/source/scriptforge/python/](https://opengrok.libreoffice.org/xref/core/wizards/source/scriptforge/python/)
