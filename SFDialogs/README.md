The **SFDialogs** folder is a subfolder of the [LibreOffice core repository tree](https://opengrok.libreoffice.org/xref/core/).

It contains the Basic source code pertaining to the SFDialogs library.

Destination: [xref: /core/wizards/source/sfdialogs/](https://opengrok.libreoffice.org/xref/core/wizards/source/sfdialogs/)
