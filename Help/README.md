The **Help** folder is a small subset of the [LibreOffice help repository tree](https://opengrok.libreoffice.org/xref/help/source/text/sbasic/shared/03/).

It lists the help files (essentially .xhp files) needed to have the ScriptForge documentation integrated nicely in a LibreOffice release.
