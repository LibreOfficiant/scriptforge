Graphical representation of the project using Unified Modeling Language (UML)
 
## Module Organization
Fig.: [Basic modules](https://gitlab.com/LibreOfficiant/scriptforge/blob/master/DocFiles/images/Basic_modules.svg)

'Ui' prefixes denote user interactions.
## Class diagrams
Fig.: [Modules & Classes](https://gitlab.com/LibreOfficiant/scriptforge/blob/master/DocFiles/images/Modules__Classes.svg)
## Work-in-Progress (WiP)
- [Scripting framework](https://gitlab.com/LibreOfficiant/scriptforge/blob/master/DocFiles/images/Scripting_Framework.svg)
- [Platform](https://gitlab.com/LibreOfficiant/scriptforge/blob/master/DocFiles/images/Platform.svg)
- [Files & Folders](https://gitlab.com/LibreOfficiant/scriptforge/blob/master/DocFiles/images/File__Folder.svg)