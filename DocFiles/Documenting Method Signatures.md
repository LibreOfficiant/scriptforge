**Documenting ScriptForge method signatures**

- Whenever ScriptForge methods are proposed solely for Basic, use the Basic syntax parser as per guidelines in [help](https://help.libreoffice.org/latest/en-US/text/sbasic/shared/conventions.html)
- Whenever methods are proposed for Python and for Basic, or solely for Python, use the programming language agnostic layout that is presented herewith.

The following typographical rules are mixing the [UML notation](https://www.uml-diagrams.org/operation.html), the API doc. layout and the [UNO object inspector](https://tomazvajngerl.blogspot.com/2021/03/built-in-xray-like-uno-object-inspector_24.html) user interface:

- indicate optional parameters with either opt, '=' default value, or '[' ']' brackets 
- arguments are lowercase, which complies with Python PEP 8 while Basic is case-agnostic
- Denote API sequences or collection-arguments using [UML multiplicity](https://www.uml-diagrams.org/multiplicity.html?context=class-diagrams)
- _Optional:_ Use 'in' / 'inout' optional specifiers to xlate byVal / byRef ( 'out' for WinAPI calls)

- Basic and Python data types should be syntactically xlated as:
 
| Basic | SYNTAX | Python |
| ------ | ------ | ------ |
| Boolean | bool | bool |
| Byte[^1] |  |  |
| Currency | num | num |
| Date | datetime | datetime |
| Double | float | float |
| Decimal[^1] | num | num |
| Integer | int | int |
| Long | int | int |
| Object | obj |  |
| Single | float | float |
| String | str | str |
| Variant | any |  |
|  |  |  |
| UNO object | uno |  |
| UDT: User defined type[^1] | obj |  |
| (class)module | obj |  |
| SF service | svc |  |

[^1]: not used yet.

**Examples:**

MsgBox( prompt : str, [ buttons = 0 ], [ title: str ] )[: str ] _OR_

MsgBox( prompt : str, buttons = MB_OK , opt title: str ) : opt str

InputBox( prompt : str, default = "", [ title: str ], [ xpostwips: int, ypostwips: int ] ): str
