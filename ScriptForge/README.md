The **ScriptForge** folder is a subfolder of the [LibreOffice core repository tree](https://opengrok.libreoffice.org/xref/core/).

It contains the Basic source code pertaining to the ScriptForge library.

Destination: [xref: /core/wizards/source/scriptforge/](https://opengrok.libreoffice.org/xref/core/wizards/source/scriptforge/)
