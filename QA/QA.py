import sys
import uno
import platform
import traceback

#   Put the user library on top of the path to force use of the scriptforge.py version in development
sys.path.reverse()
if platform.system() == 'Linux':
    sys.path.append('/home/jean-pierre/.config/libreoffice/4/user/Scripts/python/QA')
else:
    sys.path.append(r'C:\Users\jp\AppData\Roaming\LibreOffice\4\user\Scripts\python\QA')
sys.path.reverse()

from scriptforge import ScriptForge, CreateScriptService

if __name__ == '__main__':
    # Launch LibreOffice with either
    #   /opt/libreoffice24.2/program/soffice --accept='socket,host=localhost,port=2021;urp;'
    #   /opt/libreoffice24.2/program/soffice --accept="pipe,name=libreoffice;urp;"
    # ScriptForge('localhost', 2024)
    ScriptForge(pipe = 'libreoffice')
    pass

userinteraction = False
sfconsole = False
shellconsole = False
testprinter = True
printername = 'PDF'

timer = None
exc = CreateScriptService('exception')
bas = CreateScriptService('basic')
ui = CreateScriptService('ui')
statuscount = 0
attributeslist = []

def main():
    global timer
    setup()
    timer = CreateScriptService('timer', True)

    """testArray()
    testBasic()
    testDictionary()
    testException()
    testFileSystem()
    testL10N()
    testPlatform()
    testRegion()
    testSession()
    testString()
    testTextStream()
    testTimer()
    testUI()
    testDatabase()
    testDataset()
    # testDatasheet() # BROKEN 24.2
    testDocument()"""
    testCalc()
    """testChart()
    testWriter()
    testForm()
    testDialog()
    testToolbar()"""

    timer.Terminate()
    printprogress('TIMER:', timer.Duration)
    teardown()

def printprogress(*args):
    global statuscount
    cstSteps = 3000
    d = timer.duration
    statuscount += 1
    if sfconsole:
        exc.DebugPrint(d, *args)
    elif shellconsole:
        print(d, *args)
    else:
        print(d, *args)
    # ui.ShowProgressBar('Progress', str(statuscount) + '. ' + repr(args[0]), statuscount / cstSteps * 100)
    # ui.SetStatusbar(str(statuscount) + '. ' + repr(args[0]), statuscount / cstSteps * 100)

def setup():
    global exc, bas, ui
    # ScriptForge('localhost', 2021)
    ScriptForge.pythonhelpermodule2 = 'user#QA/ScriptForgeHelper.py'    # Force alternale Python helper script
    ScriptForge.InvokeSimpleScript('QA.Unit_Tests._SetTestHelper')  # Initialize root and set alternate python helper
    # Delete temporary files from previous runs
    FSO = CreateScriptService('FileSystem')
    if len(FSO.SubFolders(FSO.TemporaryFolder, 'SF_*')) > 0:
        FSO.DeleteFolder(FSO.BuildPath(FSO.TemporaryFolder, 'SF_*'))
    # Start console registration
    if sfconsole:
        exc.Console(False)
    elif shellconsole:
        exc.PythonShell({**globals(), **locals()})

def teardown():
    ui.SetStatusbar()
    ui.ShowProgressBar()
    bas.MsgBox("END OF TEST PROCESSING")


# #####################################################################################################################
# TEST ARRAY                                                                                                        ###
#######################################################################################################################
def testArray():
    import datetime
    printprogress('TEST ARRAY')
    array = CreateScriptService('array')
    FSO = CreateScriptService("FileSystem")
    FSO.FileNaming = "SYS"
    a = FSO.GetTempName()
    b = FSO.BuildPath(a, "myCSVfile.csv")
    file = FSO.CreateTextFile(b, overwrite = True)
    file.WriteLine(
        "policyID,statecode,county,eq_site_limit,hu_site_limit,fl_site_limit,fr_site_limit,tiv_2011,tiv_2012,eq_site_deductible,hu_site_deductible,fl_site_deductible,fr_site_deductible,point_latitude,point_longitude,line,construction,point_granularity")
    file.WriteLine(
        "119736,FL,CLAY COUNTY,498960,498960,498960,498960,498960,792148.9,0,9979.2,0,0,30.102261,-81.711777,Residential,""Masonry"",1")
    file.WriteLine(
        "448094,FL,CLAY COUNTY,1322376.3,1322376.3,1322376.3,1322376.3,1322376.3,1438163.57,0,0,0,0,30.063936,-81.707664,Residential,Masonry,3")
    file.WriteLine(
        "206893,FL,CLAY COUNTY,190724.4,190724.4,190724.4,190724.4,190724.4,192476.78,0,0,0,0,30.089579,-81.700455,Residential,Wood,2020/03/28")
    file.WriteLine(
        "333743,FL,CLAY COUNTY,0,79520.76,0,0,79520.76,86854.48,0,0,0,0,30.063236,-81.707703,Residential,Wood,3")
    file.WriteLine(
        "172534,FL,CLAY COUNTY,0,254281.5,0,254281.5,254281.5,246144.49,0,0,0,0,30.060614,-81.702675,Residential,Wood,1")
    file.WriteLine(
        "785275,FL,CLAY COUNTY,0,515035.62,0,0,515035.62,884419.17,0,0,0,0,30.063236,-81.707703,Residential,Masonry,3")
    file.WriteLine(
        '995932,FL,CLAY COUNTY,0,19260000,0,0,19260000,20610000,0,0,0,0,30.102226,-81.713882,Commercial,"""Reinforced"", and Concrete",1')
    file.WriteLine(
        "223488,FL,CLAY COUNTY,328500,328500,328500,328500,328500,348374.25,0,16425,0,0,30.102217,-81.707146,Residential,Wood,1")
    file.WriteLine(
        "433512,FL,CLAY COUNTY,315000,315000,315000,315000,315000,265821.57,0,15750,0,0,30.118774,-81.704613,Residential,Wood,1")
    file.WriteLine(
        "142071,FL,CLAY COUNTY,705600,705600,705600,705600,705600,1010842.56,14112,35280,0,0,30.100628,-81.703751,Residential,Masonry,1")
    file.WriteLine(
        "253816,FL,CLAY COUNTY,831498.3,831498.3,831498.3,831498.3,831498.3,1117791.48,0,0,0,0,30.10216,-81.719444,Residential,Masonry,1")
    file.CloseFile()
    c = array.ImportFromCSVFile(b, delimiter = ",", dateformat = "YYYY/MM/DD")
    FSO.DeleteFolder(a)
    printprogress('Date:', c[3][17], datetime.date.fromisoformat(c[3][17]))
    assert datetime.date.fromisoformat(c[3][17]) == datetime.date(2020, 3, 28)
    assert c[1][16] == 'Masonry'
    printprogress('Quotes:', c[7][16])
    assert c[7][16] == '"Reinforced", and Concrete'
    printprogress('Last line:', c[-1][0])
    assert isinstance(c[-1][0], int) and c[-1][0] == 253816


# #####################################################################################################################
# TEST BASIC                                                                                                        ###
#######################################################################################################################
def testBasic():
    printprogress('TEST BASIC')
    bas = CreateScriptService('Basic')
    a = 'file:///home/jean-pierre/Documents/Access2Base/Doc/Access2Base/access2base.html'
    b = bas.ConvertFromUrl(a)
    c = bas.ConvertToUrl(b)
    assert a == c
    printprogress('CreateUnoService', bas.CreateUnoService('com.sun.star.i18n.CharacterClassification'))
    printprogress('Date functions')
    d = bas.CDate('abcd')
    printprogress('CDate', 'abcd', d)
    assert d == 'abcd'
    d = bas.CDate(1001.26)
    printprogress('CDate', 1001.26, d)
    assert d.second == 24
    d = bas.CDate('2021-02-18')
    printprogress('CDate', '2021-02-18', d)
    assert d.year == 2021
    d = bas.CDateToUnoDateTime(bas.now())
    e = bas.CDateFromUnoDateTime(d)
    assert abs(bas.datediff('m', d, bas.Now())) < 2
    assert bas.datediff('d', bas.dateAdd('d', 1, bas.now()), bas.Now()) == -1
    assert bas.DatePart('yyyy', bas.now()) > 2020
    assert bas.DatePart('yyyy', bas.DateValue('2021-02-18')) == 2021
    assert bas.Format(6328.2, '##,##0.00') in ('6,328.20', '6.328,20')
    printprogress('GetDefaultContext', bas.GetDefaultContext())
    printprogress('GetGuiType', bas.getGuiType())
    printprogress('GetSystemTicks', bas.GetSystemTicks())
    assert bas.GetPathSeparator() in ('/', '\\')
    printprogress('GlobalScope')
    printprogress(bas.GlobalScope.BasicLibraries())
    printprogress(bas.GlobalScope.DialogLibraries())
    printprogress(bas.StarDesktop)
    char = bas.CreateUnoService('com.sun.star.i18n.CharacterClassification')
    printprogress(char)
    if userinteraction:
        printprogress(bas.InputBox('This is a message', xpostwips = 10, ypostwips = 10))
        printprogress(bas.MsgBox('Some text', bas.MB_ABORTRETRYIGNORE + bas.MB_ICONEXCLAMATION,
                                      'Some title'))
        bas.Xray(char)
    global attributeslist
    basd = dir(bas)
    for bb in basd:
        if bb != bb.upper() and bb[0] != bb[0].lower():
            attributeslist.extend([(bb, 'Method', bas.servicename)])

# #####################################################################################################################
# TEST DICTIONARY                                                                                                   ###
#######################################################################################################################
def testDictionary():
    import datetime
    printprogress('TEST DICTIONARY')
    d = dict(A = 1, B = (1, 2, 3, 4), C = 3)
    a = CreateScriptService('dictionary', d)
    printprogress('Preloaded dict', a)
    # a.update(d)
    now = bas.Now()
    for t in (datetime.datetime, datetime.date, datetime.time):
        if t == datetime.datetime:
            a['D'] = now
        elif t == datetime.date:
            a['D'] = now.date()
        elif t == datetime.date:
            a['D'] = now.time()
        a['D'] = now
        a['E'] = bas.createUnoService('com.sun.star.i18n.CharacterClassification')
        printprogress('Updated dict', t, a)
        assert len(a) == 5
        b = a.convertToPropertyValues()
        printprogress('Extracted PropertyValues', b)
        c = repr(a)
        a.clear()
        printprogress('Cleared dict', a)
        a.importFromPropertyValues(b)
        printprogress('Reimported PropertyValues', a)
        assert len(a) == 5
        assert c == repr(a)

# #####################################################################################################################
# TEST EXCEPTION                                                                                                    ###
#######################################################################################################################
def testException():
    printprogress('TEST EXCEPTION')
    # Nothing to do, everything is in printprogress etc.

# #####################################################################################################################
# TEST FILESYSTEM                                                                                                   ###
#######################################################################################################################
def testFileSystem():
    """
    FSO = SFScriptForge.SF_FileSystem(ScriptForge.servicesmodules['ScriptForge.FileSystem'], 'SF_FileSystem',
                                      classmodule = SFServices.moduleStandard)
        """
    FSO = CreateScriptService("FileSystem")
    printprogress('TEST FILESYSTEM')
    FSO.FileNaming = 'SYS'
    a = ('E:\ScriptForge\Help\sf_filesystem.xhp' if bas.GetGuiType() == 1
         else '/home/jean-pierre/Documents/ScriptForge/Doc/Help/sf_filesystem.xhp')
    temp = FSO.TemporaryFolder

    printprogress('PickFile')
    if userinteraction:
        b = FSO.PickFile(a, "OPEN", "xhp")
        printprogress(FSO.FileNaming, "File = ", b)

    printprogress('PickFolder')
    if userinteraction:
        b = FSO.PickFolder(FSO.GetParentFolderName(a))
        printprogress(FSO.FileNaming, "Folder = ", b)

    printprogress('FolderExists')
    assert FSO.FolderExists(a) is False
    printprogress('FileExists')
    assert FSO.fileexists(a) is True

    printprogress('Folder properties')
    printprogress('TemporaryFolder', FSO.FileNaming, FSO.TemporaryFolder)
    printprogress('HomeFolder', FSO.FileNaming, FSO.HomeFolder)
    printprogress('InstallFolder', FSO.FileNaming, FSO.InstallFolder)
    printprogress('ConfigFolder', FSO.FileNaming, FSO.configfolder)
    printprogress('ExtensionsFolder', FSO.FileNaming, FSO.ExtensionsFolder)
    pf = CreateScriptService('Platform')
    c = pf.Extensions
    for d in c:
        printprogress(d, FSO.ExtensionFolder(d))
    printprogress('TemplatesFolder', FSO.FileNaming, FSO.TemplatesFolder)
    printprogress('UserTemplatesFolder', FSO.FileNaming, FSO.UserTemplatesFolder)

    b = FSO.GetTempName()
    printprogress('GettempName', b)
    printprogress(FSO.FileNaming, temp)
    assert b.startswith(temp + 'SF_')

    FSO.FileNaming = 'URL'
    c = FSO.BuildPath(bas.ConvertToUrl(b), name = "This is a file name.old.odt")
    printprogress('BuildPath', FSO.FileNaming, "Path =", c)
    assert c.endswith("This%20is%20a%20file%20name.old.odt")

    printprogress('Files')
    FSO.FileNaming = "SYS"
    d = FSO.Files(FSO.GetParentFolderName(a), "sf_*.*")
    printprogress('Files', FSO.FileNaming, d[0][-50:])
    printprogress('Files', FSO.FileNaming, d[-1][-50:])

    printprogress('Files including subfolders')
    d = FSO.Files('/home/jean-pierre/libreoffice/libo/wizards/source', includesubfolders = True)
    for i in range(0, len(d), 25):
        printprogress(str(i), d[i])
    printprogress(len(d), 'files found')
    assert len(d) > 190

    printprogress('SubFolders')
    FSO.FileNaming = "SYS"
    c = FSO.TemporaryFolder
    FSO.CreateFolder(FSO.BuildPath(c, "SF_ANY"))
    d = FSO.SubFolders(c, "SF_*")
    printprogress('SubFolders', FSO.FileNaming, d[0])
    printprogress('SubFolders', FSO.FileNaming, d[-1], len(d) - 1)
    assert d[0].endswith(bas.GetPathSeparator())
    FSO.DeleteFolder(FSO.BuildPath(c, "SF_ANY"))

    printprogress('Subfolders including subfolders')
    d = FSO.SubFolders('/home/jean-pierre/libreoffice/libo/wizards/source', includesubfolders = True)
    for i in range(len(d)):
        printprogress(str(i), d[i])
    printprogress(len(d), 'folders found')
    assert len(d) > 20

    FSO.FileNaming = "SYS"
    b = FSO.GetTempName()
    c = FSO.BuildPath(b, name = "This is a file name.old.odt")
    printprogress('GetName', FSO.FileNaming, FSO.GetName(c))
    assert FSO.GetName(c) == "This is a file name.old.odt"

    FSO.FileNaming = "SYS"
    b = FSO.GetTempName()
    c = FSO.BuildPath(b, name = "This is a file name.old.odt")
    printprogress('GetExtension', FSO.FileNaming, FSO.GetExtension(c))
    assert FSO.GetExtension(c) == "odt"

    FSO.FileNaming = "SYS"
    b = FSO.GetTempName()
    c = FSO.BuildPath(b, name = "This is a file name.old.odt")
    printprogress('GetBaseName', FSO.FileNaming, FSO.GetBaseName(c))
    assert FSO.GetBaseName(c) == "This is a file name.old"

    FSO.FileNaming = "URL"
    b = FSO.GetTempName()
    c = FSO.BuildPath(b, name = "This is a file name.old.odt")
    printprogress('GetParentFolderName', FSO.FileNaming, FSO.GetParentFolderName(c))
    assert '/SF_' in FSO.GetParentFolderName(c)

    FSO.FileNaming = "SYS"
    b = FSO.GetTempName()
    c = FSO.BuildPath(b, "SF1")
    FSO.CreateFolder(c)
    d = FSO.BuildPath(c, FSO.GetName(a))
    printprogress('CopyFile', a[-40:], d[-40:])
    FSO.CopyFile(a, d, False)
    assert FSO.FileExists(d)
    e = FSO.BuildPath(FSO.GetParentFolderName(a), "*.xhp")
    printprogress('CopyFile', e[-40:], c[-40:])
    FSO.CopyFile(e, c, True)
    assert FSO.FileExists(FSO.BuildPath(c, "sf_array.xhp"))

    f = FSO.BuildPath(b, "SF2")
    printprogress('CopyFolder', c, f)
    FSO.CopyFolder(c, f)
    assert FSO.FileExists(FSO.BuildPath(f, "sf_array.xhp"))
    g = FSO.BuildPath(b, "SF*")
    h = FSO.BuildPath(b, "SF3")
    printprogress('CopyFolder', g, h)
    FSO.CopyFolder(g, h)
    assert FSO.FileExists(FSO.BuildPath(FSO.BuildPath(FSO.BuildPath(b, "SF3"), "SF2"), "sf_array.xhp"))

    c = FSO.BuildPath(FSO.BuildPath(b, "SF3"), "SF1")
    d = FSO.BuildPath(c, "sf_array.xhp")
    e = FSO.BuildPath(c, "sf_array2.xhp")
    printprogress('MoveFile', d, e)
    FSO.MoveFile(d, e)
    assert FSO.FileExists(e)
    f = FSO.BuildPath(b, "SF1")
    g = FSO.BuildPath(f, "*.xhp")
    h = FSO.BuildPath(b, "SF4")
    printprogress('MoveFile', g, h)
    FSO.MoveFile(g, h)
    assert FSO.FileExists(FSO.BuildPath(h, "sf_array.xhp"))

    c = FSO.BuildPath(b, "SF4")
    d = FSO.BuildPath(FSO.BuildPath(b, "SF1"), "SF4")
    printprogress('MoveFolder', c, d)
    FSO.CopyFolder(c, d) if bas.GetGuiType() == 1 else FSO.MoveFolder(c, d)
    assert FSO.FolderExists(d)
    g = FSO.BuildPath(b, "SF3")
    c = FSO.BuildPath(g, "*")
    printprogress('MoveFolder', c, d)
    FSO.CopyFolder(c, d) if bas.GetGuiType() == 1 else FSO.MoveFolder(c, d)
    assert FSO.FileExists(
        FSO.BuildPath(FSO.BuildPath(FSO.BuildPath(FSO.BuildPath(b, "SF1"), "SF4"), "SF1"), "sf_array2.xhp"))

    c = FSO.GetFileModified(a)
    printprogress('GetFileModified', c)
    assert bas.DatePart('yyyy', c) >= 2020

    c = FSO.GetFileLen(a)
    printprogress('GetFileLen', c)
    assert c > 50000

    c = FSO.BuildPath(b, "SF5")
    FSO.CreateFolder(c)
    d = FSO.BuildPath(c, FSO.GetName(a))
    FSO.CopyFile(a, d, False)
    printprogress('CompareFiles', a[-40:], d[-40:])
    assert FSO.CompareFiles(a, d, False)
    assert FSO.CompareFiles(d, a, True)

    c = FSO.BuildPath(b, "SF1/SF4/SF2")
    d = FSO.BuildPath(c, "sf_array.xhp")
    printprogress('DeleteFile', d)
    FSO.DeleteFile(d)
    assert FSO.FileExists(d) is False
    d = FSO.BuildPath(c, "*.xhp")
    printprogress('DeleteFile', d)
    FSO.DeleteFile(d)
    assert len(FSO.Files(c)) == 0    #	Empty array

    c = FSO.BuildPath(b, "SF1/SF4/")
    d = FSO.BuildPath(c, "*")
    printprogress('DeleteFolder', d)
    FSO.DeleteFolder(d)
    assert FSO.FolderExists(FSO.BuildPath(c, "SF1")) is False
    printprogress('DeleteFolder', b)
    FSO.DeleteFolder(b)
    assert FSO.FolderExists(b) is False

    FSO.FileNaming = "ANY"
    c = FSO.BuildPath(b, "SF1/SF4/afile.txt")
    printprogress(FSO.FileNaming, c)
    # assert SF_String.IsUrl(c)

    c = ('C:\Program Files\LibreOffice\program\msformslo.dll' if bas.GetGuiType() == 1
         else '/opt/libreoffice7.3/program/libbootstraplo.so')
    for d in ("MD5", "SHA1", "SHA224", "SHA256", "SHA384", "SHA512"):
        e = FSO.HashFile(c, d)
        printprogress('HashFile', c, d, e)
        assert len(e) > 0 # and SF_String.IsHexDigit(e)

    for h in FSO.Properties():
        printprogress('GetProperty', h, FSO.GetProperty(h))

    printprogress('Normalize')
    h = 'A/foo/../B/C/./D//E'
    FSO.FileNaming = 'ANY'
    printprogress(h, FSO.Normalize(h))
    FSO.FileNaming = 'SYS'
    printprogress(h, FSO.normalize(h))

    printprogress("Document's file system")
    b = FSO.GetTempName()
    printprogress('FIREBIRD', 'CreateBaseDocument')
    c = FSO.BuildPath(b, 'TestFIREBIRDdatabase.odb')
    ui = CreateScriptService('ui')
    a = ui.CreateBaseDocument(c, 'FIREBIRD')
    d = a.FileSystem
    e = FSO.SubFolders(d)
    for f in e:
        printprogress('Internal folder', f)
    printprogress('Table creation')
    sql = 'CREATE TABLE [EXPENSES]' \
          + '([CATEGORY] VARCHAR(50),[VAT CODE] SMALLINT,[COMMENT] VARCHAR(8000),[EXPENSE DATE] TIMESTAMP' \
          + ',[DESCRIPTION] VARCHAR(50),[ID EXPENSE] INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,[AMOUNT] DOUBLE PRECISION)'
    db = a.GetDatabase()
    db.RunSql(sql, directsql = True)
    db.CloseDatabase()
    a.Save()
    e = FSO.SubFolders(d, includesubfolders = True)
    for f in e:
        printprogress('Internal folder', f)
    assert FSO.FolderExists(d + 'META-INF') is True

    printprogress('TextStream')
    if not FSO.FolderExists(d + 'log'):
        FSO.CreateFolder(d + 'log')
    e = FSO.BuildPath(d + 'log', 'logfile.txt')
    for i in (1, 2):
        printprogress('Write line ', i)
        if FSO.FileExists(e):
            f = FSO.OpenTextFile(e, iomode = FSO.ForAppending)
        else:
            f = FSO.CreateTextFile(e)
        f.WriteLine('Line ' + str(i))
        f.CloseFile()

    fi = FSO.Files(d, includesubfolders = True)
    for f in fi:
        printprogress('Internal file', f)
    assert FSO.FileExists(e) is True

    a.CloseDocument(False)
    a = a.Dispose()
    FSO.DeleteFile(c)

    FSO.dispose()
    printprogress('Dispose', FSO)


# #####################################################################################################################
# TEST L10N                                                                                                         ###
#######################################################################################################################
def testL10N():
    import time
    printprogress('TEST L10N')
    a = CreateScriptService("L10N")
    FSO = CreateScriptService("FileSystem")
    FSO.FileNaming = "SYS"
    b = FSO.GetTempName()   # Tempry dir

    printprogress('AddText')
    a.AddText('', "Add a very, very long text, even longer, I %1 even further, I'm not sure to have reached" +
              "the end but arrival gets closer. Will this sentence be wrapped in the output POT file ?")
    a.AddText("key1", "Add another text", "This is the comment\nspread over two lines\twith tabulation.")
    assert a.GetText("Key1") == "Add another text"

    printprogress('ExportToPOTFile')
    FSO.CreateFolder(b)
    a.ExportToPOTFile(FSO.BuildPath(b, "SF.pot"))
    a = a.dispose()

    c = (r'E:\ScriptForge\Test\fr.po' if bas.GetGuiType() == 1
         else '/home/jean-pierre/Documents/ScriptForge/Doc/libo_multilingual_v_0_7_3/PO/fr.po')
    FSO.CopyFile(c, FSO.BuildPath(b, "fr.po"))
    a = CreateScriptService("L10N", foldername = b, locale = "fr") # This loads a quite long .po file
    d = a.GetText("RID_COMMON_START_21")
    printprogress('GetText', d)
    assert d.startswith(
            "L'assistant n'a pas pu être exécuté, car certains fichiers importants sont manquants.\\nPour rétablir les chemins")
    d = a._("RID_COMMON_START_21")
    printprogress('GetText', d)
    assert d.startswith("L'assistant n'a pas pu être exécuté, car certains fichiers importants sont manquants.\\nPour rétablir les chemins")

    printprogress('locale', a.locale)
    for h in a.Properties():
        printprogress('GetProperty', h, a.GetProperty(h))

    printprogress('Locale2 fallback')
    a1 = CreateScriptService('L10N', foldername = b, locale = 'xx', locale2 = 'fr')    #	This loads a quite long .po file
    d = a1.GetText('RID_COMMON_START_21')
    printprogress(d)
    assert d.startswith("L'assistant n'a pas pu être exécuté, car certains fichiers importants sont manquants.\\nPour rétablir les chemins")
    d = a1._('RID_COMMON_START_21')
    printprogress(d)
    assert d.startswith("L'assistant n'a pas pu être exécuté, car certains fichiers importants sont manquants.\\nPour rétablir les chemins")
    a1.Dispose()

    printprogress('AddTextsFromDialog')
    e = CreateScriptService("dialog", "GlobalScope", "QA", "QADialog")
    a.AddTextsFromDialog(e)

    printprogress('GetTextsFromL10N')
    e.GetTextsFromL10N(a)
    e.Execute(False)
    time.sleep(1)
    e.Terminate()
    e.Dispose()

    FSO.DeleteFolder(b) # PUT BREAKPOINT HERE TO SEE TEMPRY FOLDER CONTENT
    a = a.dispose()


# #####################################################################################################################
# TEST PLATFORM                                                                                                     ###
#######################################################################################################################
def testPlatform():
    printprogress('TEST PLATFORM')
    pf = CreateScriptService('Platform')
    printprogress(pf.architecture)
    printprogress(pf.computerName)
    printprogress(pf.CPUCount)
    printprogress(pf.CurrentUser)
    a = pf.FilterNames
    for i in range(10):
        printprogress(a[i])
    a = pf.Fonts
    for i in range(10):
        printprogress(a[i])
    printprogress(pf.FormatLocale)
    printprogress(pf.Locale)
    printprogress(pf.Machine)
    printprogress(pf.OfficeLocale)
    printprogress(pf.OfficeVersion)
    printprogress(pf.OSName)
    printprogress(pf.osPlatform)
    printprogress(pf.OSRelease)
    printprogress(pf.OSVersion)
    printprogress(pf.Printers)
    printprogress(pf.Processor)
    printprogress(pf.PythonVersion)
    printprogress(pf.SystemLocale)
    d = pf.UserData
    printprogress('\n'.join('{0} {1}'.format(k, v) for k, v in d.items()))
    pf.dispose()


# #####################################################################################################################
# TEST PLATFORM                                                                                                     ###
#######################################################################################################################
def testRegion():
    printprogress('TEST REGION')
    bas = CreateScriptService('Basic')
    reg = CreateScriptService('Region')

    printprogress('Currency', reg.Currency('IT'))
    printprogress('Language', reg.Language('IT'))
    printprogress('Country', reg.Country('IT'))

    printprogress('DatePatterns', reg.DatePatterns('it-IT'))
    printprogress('DateSeparator', reg.DateSeparator('it-IT'))

    printprogress('ThousandSeparator', reg.ThousandSeparator('it-IT'))
    printprogress('DecimalPoint', reg.DecimalPoint('it-IT'))
    printprogress('TimeSeparator', reg.TimeSeparator('it-IT'))
    printprogress('ListSeparator', reg.ListSeparator('it-IT'))

    printprogress('DayNames', ','.join(reg.DayNames('it')))
    printprogress('DayAbbrevNames', ','.join(reg.DayAbbrevNames('it')))
    printprogress('DayNarrowNames', ','.join(reg.DayNarrowNames('it')))
    printprogress('MonthNames', ','.join(reg.MonthNames('it')))
    printprogress('MonthAbbrevNames', ','.join(reg.MonthAbbrevNames('it')))
    printprogress('MonthNarrowNames', ','.join(reg.MonthNarrowNames('it')))

    d = 'Europe/Brussels'
    e = 'fr-BE'
    b = bas.Now()
    c = reg.UTCNow(d, e)
    printprogress('Now', b)
    printprogress('UTCNow', c)

    f = reg.UTCDateTime(b, d, e)
    printprogress('UTCDateTime', f)
    g = reg.LocalDateTime(f, d, e)
    printprogress('LocalDateTime', g)
    # assert g == b
    printprogress('TimeZoneOffset', reg.TimeZoneOffset(d, e))
    printprogress('DSTOffset', reg.DSTOffset(b, d, e))

    printprogress('Number2Text')
    printprogress('help', reg.Number2Text('help', 'fr'))
    printprogress('79,93', reg.Number2Text('79,93', 'fr-BE'))
    printprogress(3.14159, reg.Number2Text(3.14159, 'pt-BR'))
    printprogress('EUR 1234.56', reg.Number2Text('EUR 1234.56', 'it'))


# #####################################################################################################################
# TEST SESSION                                                                                                      ###
# #####################################################################################################################
def testSession():
    printprogress('TEST STRING')
    a = CreateScriptService('Session')

    b = a.ExecuteBasicScript('', 'Access2Base.Application.ProductCode')
    printprogress('ExecuteBasicScript', b)
    assert b[:11] == 'Access2Base'
    b = a.ExecuteBasicScript(None, 'Access2Base.Application.Version')
    printprogress('ExecuteBasicScript', b)
    assert b[:11] == 'LibreOffice'
    b = a.ExecuteBasicScript('', 'Access2Base.Application.HtmlEncode',
                             'Amounts in €, not in £. Montants affichés en €, pas en £.')
    printprogress('ExecuteBasicScript', b)
    assert '&euro;' in b

    b = a.ExecuteCalcFunction('NOW')
    printprogress('ExecuteCalcFunction', 'NOW', b)
    b = a.ExecuteCalcFunction("AVERAGE", 1, 3, 5, 7)
    printprogress('ExecuteCalcFunction', 'AVERAGE', b)
    assert b == 4.0
    b = a.ExecuteCalcFunction("ARABIC", "MXIV")
    printprogress('ExecuteCalcFunction', 'ARABIC', b)
    assert b == 1014.0
    b = a.ExecuteCalcFunction("ABS", ((-1, 2, 3), (4, -5, 6), (7, 8, -9)))
    printprogress('ExecuteCalcFunction', 'ABS', b)
    assert b[2][2] == 9
    table1 = ('Symbol', 'H', 'He', 'Li')
    table2 = ('AtomicNumber', 1.0, 2.0, 3.0)
    table3 = ('Mass', 1.008, 4.0026, 6.94)
    table = (table1, table2, table3)
    search = ('Symbol', 'AtomicNumber', 'Mass')
    b = a.ExecuteCalcFunction('XLOOKUP', 'Mass', search, table)
    printprogress('ExecuteCalcFunction', 'XLOOKUP', b)
    assert b[0][3] == 6.94

    b = a.ExecutePythonScript(a.SCRIPTISSHARED, 'Capitalise.py$getNewString', 'Abc')
    printprogress('ExecutePythonScript', 'Capitalise.py$getNewString')
    assert b == 'abc'

    b = 'www.google.com'
    c = a.WebService(b)
    printprogress('WebService', c[:50])
    assert c.startswith('<!doctype html><html itemscope=')

    c = a.ExecuteBasicScript('', 'ScriptForge.SF_Utils._GetUNOService', 'FunctionAccess')
    b = a.UnoObjectType(c)
    printprogress('UnoObjectType', b)
    assert b == 'stardiv.StarCalc.ScFunctionAccess'
    c = a.ExecuteBasicScript('', 'ScriptForge.SF_Utils._GetUNOService', 'PathSettings')
    b = a.UnoObjectType(c)
    printprogress('UnoObjectType', b)
    assert b == 'com.sun.star.comp.framework.PathSettings'
    c = a.ExecuteBasicScript('', 'ScriptForge.SF_Utils._GetUNOService', 'ScriptProvider')
    b = a.UnoObjectType(c)
    printprogress('UnoObjectType', b)
    assert b == 'com.sun.star.script.provider.MasterScriptProvider'
    c = a.ExecuteBasicScript('', 'ScriptForge.SF_Utils._GetUNOService', 'SystemShellExecute')
    b = a.UnoObjectType(c)
    printprogress('UnoObjectType', b)
    assert b == 'com.sun.star.comp.system.SystemShellExecute' or b == 'com.sun.star.sys.shell.SystemShellExecute'
    c = a.ExecuteBasicScript('', 'ScriptForge.SF_Utils._GetUNOService', 'CoreReflection')
    b = a.UnoObjectType(c)
    printprogress('UnoObjectType', b)
    assert b == 'com.sun.star.comp.stoc.CoreReflection'
    c = uno.createUnoStruct("com.sun.star.beans.Property")
    b = a.UnoObjectType(c)
    printprogress('UnoObjectType', b)
    assert b == 'com.sun.star.beans.Property'

    c = a.ExecuteBasicScript('', 'ScriptForge.SF_Utils._GetUNOService', 'FunctionAccess')
    printprogress('HasUnoProperty')
    assert a.HasUnoProperty(c, 'Wildcards')

    printprogress('PDF Options')
    d = a.GetPDFExportOptions()
    d['Zoom'] = 200
    d['Magnification'] = 4
    a.setPDFExportOptions(d)
    d = d.Dispose()
    d = a.GetPDFExportOptions()
    [printprogress(key, ':', value) for key, value in d.items()]
    assert d['Zoom'] == 200 and d['Magnification'] == 4
    d['Zoom'] = 100
    d['Magnification'] = 0
    a.setPDFExportOptions(d)
    d = d.Dispose()

    printprogress('HasUnoMethod')
    assert a.HasUnoMethod(c, 'callFunction')

    printprogress('UnoProperties')
    assert 'Wildcards' in a.UnoProperties(c)

    printprogress('UnoMethods')
    assert 'callFunction' in a.UnoMethods(c)

    printprogress('SendMail')
    if userinteraction:
        a.SendMail('aaa@bbb.be', 'bbb@ccc.be, ddd@eee.eu', 'eee@fff.fr, fff@ggg.ch'
                   , filenames = '/home/jean-pierre/Documents/ScriptForge/Doc/Help/sf_timer.xhp'
                                  + ', /home/jean-pierre/Documents/ScriptForge/Doc/Help/sf_filesystem.xhp'
                   , subject = 'This is the subject'
                   , body = 'This is the body.' + '\n' + '2nd line'
                   , editmessage = True
                   )

    printprogress('RunApplication')
    if userinteraction:
        a.RunApplication('kate', '/home/jean-pierre/Install/install.txt')

    printprogress('OpenURLInBrowser')
    if userinteraction:
        a.OpenURLInBrowser('www.access2base.com')


# #####################################################################################################################
# TEST STRING                                                                                                       ###
# #####################################################################################################################
def testString():
    printprogress('TEST STRING')
    SF_String = CreateScriptService('String')


    a = '2019-12-31'
    b = SF_String.IsADate(a, 'YYYY-MM-DD')
    printprogress('IsADate', a, b)
    assert b

    c = "œ∑¡™£¢∞§¶•ªº–≠œ∑´®†¥¨ˆøπ“‘åß∂ƒ©˙∆˚¬"
    for d in ("MD5", "SHA1", "SHA224", "SHA256", "SHA384", "SHA512"):
        e = SF_String.HashStr(c, d)
        printprogress('HashStr', c, d, e)
        assert len(e) > 0 and (e == '616eb9c513ad07cd02924b4d285b9987' or d != 'MD5')

    a = "first.last@something.org"
    b = SF_String.IsEmail(a)
    printprogress('IsEmail', a, b)
    assert b

    printprogress('IsFileName')
    assert SF_String.IsFileName('C:\a\b\de f.ods', 'WINdows')
    assert SF_String.IsFileName('/home/my file.odt', 'LINUX')
    assert SF_String.IsFileName('/home/my file.odt')
    
    printprogress('IsIBAN')
    assert SF_String.IsIBAN('BE71 0961 2345 6769')
    assert SF_String.IsIBAN('BR15 0000 0000 0000 1093 2840 814 P2')
    assert SF_String.IsIBAN('FR76 3000 6000 0112 3456 7890 189')
    assert SF_String.IsIBAN('DE91 1000 0000 0123 4567 89')
    assert SF_String.IsIBAN('GR96 0810 0010 0000 0123 4567 890')
    assert SF_String.IsIBAN('MU43 BOMM 0101 1234 5678 9101 000 MUR')
    assert SF_String.IsIBAN('PK70 BANK 0000 1234 5678 9000')
    assert SF_String.IsIBAN('PL10 1050 0099 7603 1234 5678 9123')
    assert SF_String.IsIBAN('RO09 BCYP 0000 0012 3456 7890')
    assert SF_String.IsIBAN('LC14 BOSL 1234 5678 9012 3456 7890 1234')
    assert SF_String.IsIBAN('SA44 2000 0001 2345 6789 1234')
    assert SF_String.IsIBAN('ES79 2100 0813 6101 2345 6789')
    assert SF_String.IsIBAN('CH56 0483 5012 3456 7800 9')
    assert SF_String.IsIBAN('GB98 MIDL 0700 9312 3456 78')

    printprogress('IsIPv4')
    assert SF_String.IsIPv4("192.168.1.50")
    assert SF_String.isipv4("192.168.1.256") is False

    printprogress('IsLike')
    assert SF_String.IsLike('aAbB', '?A*')
    assert SF_String.IsLike('C:\\a\\b\\c\\f.odb', '?:*.*')

    printprogress('IsSheetName')
    assert SF_String.IsSheetName('1àbc + "def"')

    printprogress('IsUrl')
    assert SF_String.IsUrl('http://foo.bar/?q=Test%20URL-encoded%20stuff')
    assert SF_String.IsUrl('http//foo.bar/?q=Test%20URL-encoded%20stuff') is False

    printprogress('SplitNotQuoted')
    a = 'abc def ghi'  # returns ("abc", "def", "ghi")
    assert SF_String.SplitNotQuoted(a)[2] == 'ghi'
    a = 'abc,"def,ghi"'    #  returns ("abc", """def,ghi""")
    assert SF_String.SplitNotQuoted(a, ',')[1] == '"def,ghi"'
    a = 'abc,"def\\", ghi"'	# returns ("abc", """def \"", ghi""")
    assert SF_String.SplitNotQuoted(a, ',')[1] == '"def\\", ghi"'
    a = 'abc,"def\\,ghi"",'    # returns ("abc", """def\"",ghi""", "")
    assert SF_String.SplitNotQuoted(a, ',')[2] == ''
    assert len(SF_String.SplitNotQuoted(a, ',', 2)) == 2

    a = 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...'
    b = SF_String.Wrap(a, 20)
    assert b[5] == 'adipisci velit...'
    for c in b:
        printprogress('Wrap', c)

# #####################################################################################################################
# TEST TEXTSTREAM                                                                                                   ###
#######################################################################################################################
def testTextStream():
    printprogress('TEST TEXTSTREAM')
    a = ('E:\ScriptForge\Help\sf_filesystem.xhp' if bas.GetGuiType() == 1
         else '/home/jean-pierre/Documents/ScriptForge/Doc/Help/sf_filesystem.xhp')
    FSO = CreateScriptService("FileSystem")
    FSO.FileNaming = "SYS"
    b = FSO.GetTempName()
    FSO.CreateFolder(b)
    c = FSO.BuildPath(b, "stream.txt")
    FSO.CopyFile(a, c, overwrite = True)

    f = FSO.OpenTextFile(c, FSO.ForReading)
    printprogress('OpenTextFile', f.FileName, f.iomode, f.Encoding, f.newLine)
    assert f.IOMode == 'READ'

    while f.atEndOfStream is False:
        d = f.ReadLine()
    printprogress('ReadLine', f.Line)
    f.CloseFile()

    f = FSO.OpenTextFile(c, FSO.ForReading)
    f.SkipLine()
    f.SkipLine()
    d = f.ReadAll()
    printprogress('ReadAll', len(d))
    assert len(d) > 1000
    f.CloseFile()

    f = FSO.OpenTextFile(c, FSO.ForAppending)
    printprogress('WriteBlankLines')
    f.WriteBlankLines(5)
    f.writeline("Last line")
    f.CloseFile()
    f = FSO.OpenTextFile(c, FSO.ForReading)
    while f.AtEndOfStream is False:
        d = f.ReadLine()
    f.CloseFile()
    printprogress('WriteLine', d)
    assert d == 'Last line'

    c = FSO.BuildPath(b, "createstream.txt")
    f = FSO.CreateTextFile(c)
    h = 10
    for i in range(1, h + 1):
        f.WriteLine("Line #" + str(i))
    f.CloseFile()
    f = FSO.OpenTextFile(c)
    while f.AtEndOfStream is False:
        d = f.ReadLine()
    printprogress('CreateTextFile', f.Line, d)
    assert d.startswith('Line #') and f.Line == h

    for h in f.Properties():
        printprogress('GetProperty', h, f.GetProperty(h))
    assert f.SetProperty('NewLine', '\r\n')
    printprogress('SetProperty', "NewLine", f.NewLine)

    # printprogress('_repr', SF_String.Represent(f)[-50:])
    f.CloseFile()
    FSO.DeleteFolder(b)


# #####################################################################################################################
# TEST TIMER                                                                                                        ###
#######################################################################################################################
def testTimer():
    printprogress('TEST TIMER')
    import time
    a = CreateScriptService("Timer")
    a.start()
    time.sleep(0.2)
    a.suspend()
    printprogress('IsSuspended', a.issuspended, a.suspendduration)
    time.sleep(0.2)
    assert a.isSuspended
    a.Continue()
    for h in a.Properties():
        printprogress('GetProperty', h, a.getProperty(h))
    time.sleep(0.2)
    a.terminate()
    printprogress('Durations', a.Duration, a.SuspendDuration, a.TotalDuration)
    a.dispose()


# #####################################################################################################################
# TEST UI                                                                                                           ###
#######################################################################################################################
def testUI():
    printprogress('TEST UI')
    FSO = CreateScriptService("FileSystem")
    ui = CreateScriptService("UI")

    printprogress('Maximize, Minimize, Resize')
    ui.Activate('BASICIDE')
    ui.Minimize()
    ui.Maximize()
    ui.Resize()
    printprogress('Size & Position', ui.Height, ui.Width, ui.X, ui.Y)

    printprogress('CreateDocument')
    a = ui.CreateDocument('Calc')
    e = ui.CreateDocument('', FSO.BuildPath(FSO.TemplatesFolder, 'personal/CV.ott'))
    c = ui.activeWindow

    printprogress('CreateBaseDocument')
    dbf = FSO.BuildPath(FSO.TemporaryFolder, 'SF_XXX/abcd.odb')
    dba = ui.CreateBaseDocument(dbf, 'firebird')
    printprogress(ui.ActiveWindow)

    printprogress('Documents')
    b = ui.Documents()
    for i in range(len(b)):
        printprogress(str(i) + '. ', b[i])
    printprogress(c)
    assert c in b
    assert dbf in b
    dba.CloseDocument()

    printprogress('WindowExists')
    assert ui.WindowExists(c)

    printprogress("OpenDocument")
    f = FSO.BuildPath(FSO.TemporaryFolder, 'SF_XXX/FileA.ods')
    a.SaveAs(f, overwrite = False)
    a.SaveAs(f, overwrite = True)
    a.CloseDocument()
    a = ui.OpenDocument(f, readonly = True)
    a.CloseDocument()

    printprogress('OpenBaseDocument')
    dba = ui.OpenBaseDocument(dbf)

    printprogress('GetDocument')
    docs = ui.Documents()
    if 'Untitled 2' in docs:
        c = 'Untitled 2'
    elif 'Untitled 3' in docs:
        c = 'Untitled 3'
    d = ui.GetDocument(c)
    assert d is not None
    dbd = ui.GetDocument('abcd.odb')
    assert dbd is not None

    printprogress('RunCommand')
    g = ui.ActiveWindow
    ui.RunCommand('BasicIDEAppear', Document = 'My Macros & Dialogs', LibName = 'QA', Name = '_UI', Line = 63)
    ui.Activate(g)

    printprogress('CloseDocument')
    d.CloseDocument()
    assert ui.WindowExists(c) is False
    a.CloseDocument()
    dbd.CloseDocument()
    assert ui.WindowExists('abcd.odb') is False

    FSO.DeleteFile(f)
    FSO.DeleteFile(dbf)

    printprogress('ActiveWindow', ui.ActiveWindow)

    printprogress('Dispose')
    ui = ui.Dispose()


# #####################################################################################################################
# TEST CALC                                                                                                         ###
#######################################################################################################################
def testCalc():
    import random, time
    printprogress('TEST CALC')
    FSO = CreateScriptService("FileSystem")
    UI = CreateScriptService("UI")
    SESS = CreateScriptService("Session")
    BASIC = CreateScriptService("Basic")

    FSO.FileNaming = 'SYS'
    b = FSO.GetTempName()
    c = FSO.BuildPath(b, 'myDocumentA.ods')

    printprogress('CreateDocument')
    a = UI.CreateDocument('Calc')
    printprogress('IsAlive', a.IsAlive)

    printprogress('SaveAs')
    a.SaveAs(c, overwrite = True)

    """printprogress('Export/Import filters')
    q = a.ExportFilters
    for e in q:
        printprogress('Export filter', e)
    q = a.ImportFilters
    for e in q:
        printprogress('Import filter', e)"""

    printprogress('MenuHeaders', a.MenuHeaders)

    printprogress('InsertSheet')
    a.InsertSheet('NewSheet', 'Sheet1')
    # z = a.XSpreadsheet('NewSheet')   # <<<<<<<<<<<<<<<<<<<<<<<,
    # printprogress('XSpreadsheet', z)
    assert 'NewSheet' in a.Sheets

    printprogress('Activate')
    a.Activate('NewSheet')
    d = a.Sheet('~')
    # assert d.SheetName == 'NewSheet'

    printprogress('CopySheet')
    a.CopySheet('NewSheet', '$', 1)
    printprogress('List of sheets', a.Sheets)
    assert '$' in a.Sheets
    e = UI.CreateDocument('Calc')
    a.CopySheet(e.Sheet('~'), 'SheetX', 1)
    #	Copy active sheet
    g = 'SheetX'    # e.Sheet('~').SheetName
    assert 'SheetX' in a.Sheets
    f = FSO.BuildPath(b, 'myDocumentE.ods')
    e.SaveAs(f, overwrite = True)
    e.CloseDocument()

    printprogress('CopySheetFromFile')
    a.CopySheetFromFile(f, g, 'SheetZZ')
    assert 'SheetZZ' in a.Sheets

    printprogress('GetColumnName')
    assert a.GetColumnName(1024) == 'AMJ'

    printprogress('MoveSheet')
    a.MoveSheet('SheetX')
    assert 'SheetX' in a.Sheets

    printprogress('RenameSheet')
    a.RenameSheet('SheetX', 'SheetZ')
    assert 'SheetZ' in a.Sheets

    printprogress("Sheet name stress tests")
    p = a.SetValue("'$'.A1", 20)
    assert a.GetValue(p) == 20
    printprogress("Address = ", p)
    a.RenameSheet("$", "$Sheet99")
    p = a.SetValue("'$Sheet99'.A1", 21)
    assert a.GetValue(p) == 21
    printprogress("Address = ", p)
    a.RenameSheet("$Sheet99", "$Sheet'99")
    p = a.SetValue("'$Sheet''99'.A1", 22)
    assert a.GetValue(p) == 22
    printprogress("Address = ", p)
    a.RenameSheet("$Sheet'99", "$.Sheet'99")
    p = a.SetValue("'$.Sheet''99'.A1", 23)
    assert a.GetValue(p) == 23
    printprogress("Address = ", p)
    a.RenameSheet("$.Sheet'99", "!'@#$%^&()-_=+{}|;,<.>""")
    p = a.SetValue("'!''@#$%^&()-_=+{}|;,<.>""'.A1", 24)
    assert a.GetValue(p) == 24
    printprogress("Address = ", p)

    printprogress('RemoveSheet')
    a.RemoveSheet('SheetZ')
    assert 'SheetZ' not in a.Sheets

    printprogress('SetValue, SetArray, GetValue')
    a.SetValue('SheetZZ.A1:D2', ((1, 2, 3, 4), (5, 6, 7, 8)))
    assert a.GetValue('SheetZZ.A1:D1')[3] == 4
    assert a.GetValue('SheetZZ.A1:D2')[1][3] == 8
    a.SetArray('SheetZZ.E1', tuple(range(1, 1001)))
    assert a.GetValue('SheetZZ.E1000') == 1000
    h = a.GetValue('SheetZZ.*')
    assert h[-1][-1] == 1000

    printprogress('Offset')
    h = a.Offset('SheetZZ.A1:D2', 999, 4, 1, 1)
    assert a.GetValue(h) == 1000

    printprogress('XSpreadsheet')   # <<<<<<<<<<<<<<<<<<<<
    # assert SESS.UnoObjectType(a.XSpreadsheet('SheetZZ')) == 'ScTableSheetObj'

    printprogress('Height, Width, Last XXX')
    assert a.Height('SheetZZ.A1:Z100') == 100
    assert a.Width('SheetZZ.A1:Z100') == 26
    printprogress('Height', a.Height('SheetZZ'), a.Height('SheetZZ.B3:Z100'))
    printprogress('Width', a.Width('SheetZZ'), a.Width('SheetZZ.B3:Z100'))
    printprogress('FirstRow', a.FirstRow('SheetZZ'), a.FirstRow('SheetZZ.B3:Z100'))
    printprogress('FirstColumn', a.FirstColumn('SheetZZ'), a.FirstColumn('SheetZZ.B3:Z100'))
    printprogress('LastRow', a.LastRow('SheetZZ'), a.LastRow('SheetZZ.B3:Z100'))
    printprogress('LastColumn', a.LastColumn('SheetZZ'), a.LastColumn('SheetZZ.B3:Z100'))
    printprogress('LastCell', a.LastCell('SheetZZ'), a.LastCell('SheetZZ.B3:Z100'))
    printprogress('SheetName', a.SheetName('SheetZZ'), a.SheetName('SheetZZ.B3:Z100'))

    printprogress('XCellRange') # <<<<<<<<<<<<<<<<<<<<<
    # assert SESS.UnoObjectType(a.XCellRange('SheetZZ.A1:Z100')) == 'ScCellRangeObj'

    printprogress('XRectangle')
    a.CurrentSelection = 'SheetZZ.E1000'
    e = a.XRectangle('SheetZZ.E1000')
    printprogress(e.X, e.Y, e.Width, e.Height)
    assert e.X > 0

    printprogress('CurrentSelection')
    a.CurrentSelection = 'SheetZZ.E1000'
    assert a.GetValue(a.CurrentSelection) == 1000
    a.CurrentSelection = ('SheetZZ.A1', 'SheetZZ.E1:E1000')
    printprogress((';').join(a.CurrentSelection))
    assert (';').join(a.CurrentSelection) == '$SheetZZ.$A$1;$SheetZZ.$E$1:$E$1000'

    printprogress('CopyToCell')
    a.InsertSheet('SheetYY', 'SheetZZ')
    h = a.CopyToCell('SheetZZ.E1:E1000', 'SheetYY.B3:B5')
    assert h == '$SheetYY.$B$3:$B$1002'
    e = UI.CreateDocument('Calc')
    h = e.CopyToCell(a.Range('SheetZZ.E1:E1000'), '~.B3:B6')
    assert '$B$3:$B$1002' in h

    printprogress('CopyToRange')
    e.InsertSheet('SheetXYZ', 1)
    h = e.CopyToRange(a.Range('SheetZZ.E1:E1000'), 'SheetXYZ.A1:B1')
    assert ':$B$1000' in h
    e.CopyToRange('SheetXYZ.A3:B3', 'SheetXYZ.C3:E6')
    assert e.GetValue('SheetXYZ.E6') == 3

    printprogress('ClearValues/Formats/All')
    e.ClearFormats('SheetXYZ.*')
    e.ClearValues('SheetXYZ.*')
    e.ClearAll('SheetXYZ.*')

    printprogress('Filter formulas')
    h = 'SheetXYZ.B2:K100'
    e.SetFormula(h, '=RANDBETWEEN.NV(-100;100)')
    e.SetCellStyle(h, 'Bad', '=(MOD(B2;2)=0)', 'COLUMN')
    e.SetCellStyle(h, 'Bad', '=(MOD(B2;2)=0)', 'ROW')
    e.SetCellStyle(h, 'Bad', '=(B2<0)', 'CELL')
    time.sleep(0.5)
    e.ClearFormats(h, '=(MOD(B2;2)=0)', 'COLUMN')
    e.ClearValues(h, '=(MOD(B2;2)=0)', 'ROW')
    e.ClearAll(h, '=(B2<0)', 'CELL')

    printprogress('MoveRange')
    h = e.MoveRange(h, '~.D3:D6')
    assert '$D$3' in h

    printprogress('PrintOut')
    if testprinter:
        printprogress('SetPrinter')
        a.SetPrinter(printer = printername, paperformat = 'Tabloid', orientation = 'Landscape')
        printprogress('Print to printer ' + printername)
        a.PrintOut('SheetZZ', pages = '2-3', copies = 2)

    e.CloseDocument(False)
    
    printprogress('ImportFromCSVFile')
    c = FSO.BuildPath(b, 'myCSVfile.csv')
    file = FSO.CreateTextFile(c, overwrite = True)
    file.WriteLine(
        'policyID,statecode,county,eq_site_limit,hu_site_limit,fl_site_limit,fr_site_limit,tiv_2011,tiv_2012,eq_site_deductible,hu_site_deductible,fl_site_deductible,fr_site_deductible,point_latitude,point_longitude,line,construction,point_granularity')
    file.WriteLine(
        '119736,FL,CLAY COUNTY,498960,498960,498960,498960,498960,792148.9,0,9979.2,0,0,30.102261,-81.711777,Residential,"Masonry",1')
    file.WriteLine(
        '448094,FL,CLAY COUNTY,1322376.3,1322376.3,1322376.3,1322376.3,1322376.3,1438163.57,0,0,0,0,30.063936,-81.707664,Residential,Masonry,3')
    file.WriteLine(
        '206893,FL,CLAY COUNTY,190724.4,190724.4,190724.4,190724.4,190724.4,192476.78,0,0,0,0,30.089579,-81.700455,Residential,Wood,2020/03/28')
    file.WriteLine(
        '333743,FL,CLAY COUNTY,0,79520.76,0,0,79520.76,86854.48,0,0,0,0,30.063236,-81.707703,Residential,Wood,3')
    file.WriteLine(
        '172534,FL,CLAY COUNTY,0,254281.5,0,254281.5,254281.5,246144.49,0,0,0,0,30.060614,-81.702675,Residential,Wood,1')
    file.WriteLine(
        '785275,FL,CLAY COUNTY,0,515035.62,0,0,515035.62,884419.17,0,0,0,0,30.063236,-81.707703,Residential,Masonry,3')
    file.WriteLine(
        '995932,FL,CLAY COUNTY,0,19260000,0,0,19260000,20610000,0,0,0,0,30.102226,-81.713882,Commercial,"""Reinforced"", and Concrete",1')
    file.WriteLine(
        '223488,FL,CLAY COUNTY,328500,328500,328500,328500,328500,348374.25,0,16425,0,0,30.102217,-81.707146,Residential,Wood,1')
    file.WriteLine(
        '433512,FL,CLAY COUNTY,315000,315000,315000,315000,315000,265821.57,0,15750,0,0,30.118774,-81.704613,Residential,Wood,1')
    file.WriteLine(
        '142071,FL,CLAY COUNTY,705600,705600,705600,705600,705600,1010842.56,14112,35280,0,0,30.100628,-81.703751,Residential,Masonry,1')
    file.WriteLine(
        "253816,FL,CLAY COUNTY,831498.3,831498.3,831498.3,831498.3,831498.3,1117791.48,0,0,0,0,30.10216,-81.719444,Residential,Masonry,1")
    file.CloseFile()
    g = 12  # g = FSO._CountTextLines(c)
    # PrintProgress("CSV: " & g & " lines")
    e = UI.CreateDocument("Calc")
    f = e.ImportFromCSVFile(c, 'C3')
    printprogress(f)
    assert e.GetValue(e.Offset('C3', 3, 17)) == '2020/03/28'
    assert e.GetValue(e.Offset('C3', 1, 16)) == 'Masonry'
    assert e.GetValue(e.Offset('C3', 7, 16)) == '"Reinforced", and Concrete'
    assert e.GetValue(e.Offset('C3', g - 1, 0)) == 253816

    printprogress('Get/SetFormula')
    e.SetFormula('B4', '=Sum(C4:T4')
    assert e.GetValue('B4') > 119736
    assert e.GetFormula('B4') == '=SUM(C4:T4)'
    e.SetFormula('B5:B6', ('=25-10', '=20-B5'))
    assert e.GetValue('B6') == 5
    e.SetFormula(e.Offset('B4', 0, 0, g - 1, 1), '=Sum(C4:T4')
    assert e.GetValue('B14') > 5000000

    printprogress('SortRange')
    f = e.SortRange('C3:T14', (3, 9), ('', 'DESC'), 'C20', containsheader = True)
    assert e.GetValue('T30') == '2020/03/28'
    printprogress(f)
    h = ['A' + str(i) for i in range(12)]
    e.SetArray('B20', h)
    f = e.SortRange('B20:T31', 1, 'DESC', containsheader = True, casesensitive = True)
    assert e.GetValue('B31') == 'A1'

    printprogress('SetCellStyle')
    e.SetCellStyle('B20:T20', 'Heading 2')
    e.SetCellStyle('B21:T31', 'Neutral')

    printprogress('ExportRangeToFile')
    p = 'Sheet1.B20:T31'
    for t in ('pdf', 'jpeg', 'png'):
        r = FSO.BuildPath(b, 'export.' + t)
        printprogress(t, r)
        e.ExportRangeToFile(p, r, imagetype = t, overwrite = True)

    printprogress('DFunctions')
    printprogress('Avg = ', e.DAvg('B20:T31'))
    printprogress('Count = ', e.DCount('B20:T31'))
    printprogress('Max = ', e.DMax('B20:T31'))
    printprogress('Min = ', e.DMin('B20:T31'))
    printprogress('Sum = ', e.DSum('B20:T31'))
    assert e.DSum('B20:T31') / e.DCount('B20:T31') == e.DAvg('B20:T31')

    printprogress("A1Style")
    printprogress(e.A1Style(3, 2))
    printprogress(e.A1Style(3, 2, 10, 10))
    printprogress(e.A1Style(10, 10, 3, 2, "Sheet1"))
    printprogress(e.A1Style(20, 2, 31, 20, "~"))
    printprogress(e.DSum(e.a1Style(20, 2, 31, 20, "~")))

    printprogress('Intersect')
    printprogress(e.Intersect('J7:M11', '$Sheet1.$L$10:$N$17'))
    assert len(e.Intersect('J7:M11', '$Sheet1.$L$15:$N$17')) == 0

    printprogress('Printf')
    printprogress(e.printf('$_S.$_C1$_R1:$_C2$_R2__', 'A10:Z20', tokencharacter = '_'))

    printprogress('OpenRangeSelector')
    if userinteraction:
        printprogress(e.OpenRangeSelector('Select a range ...'))
    else:
        printprogress('Not tested')

    printprogress("Region")
    printprogress(e.Region("Sheet1.$E$22"))
    printprogress(e.Region("Sheet1.$E$15:$G$22"))

    printprogress('XSheetCellCursor')   # <<<<<<<<<<<<<<<<<<<<<
    """f = e.XSheetCellCursor('Sheet1.$E$15:$G$22')
    printprogress(f, 'Sheet1.$E$15:$G$22')
    f.gotoNext()
    printprogress(f.AbsoluteName)"""

    printprogress('ShiftDown/Left/Right/Up')
    p = 'Sheet1.B20:T31'
    p = e.ShiftRight(p, columns = 3)
    printprogress(p)
    p = e.ShiftDown(p, rows = 3)
    printprogress(p)
    p = e.ShiftLeft(p, columns = 3)
    printprogress(p)
    p = e.ShiftUp(p, rows = 3)
    printprogress(p)
    printprogress(e.ShiftUp(p, wholerow = True))

    e.CloseDocument(False)

    printprogress('Create [Calc] BaseDocument')
    a.Save()
    a.CloseDocument()
    c = FSO.BuildPath(b, 'myDocumentA.ods')
    dba = ui.CreateBaseDocument(FSO.BuildPath(b, 'MyCalcDb.odb'), 'CALC', '', c)
    printprogress('Tables', dba.GetDatabase().Tables)
    dba.CloseDocument(False)
    dba = dba.Dispose()

    printprogress('CompactUp / CompactLeft')
    e = UI.CreateDocument('Calc')
    p = ()
    for i in range(0, 100):
        q = ()
        for j in range(0, 40):
            q += (random.randrange(0, 10),)
        p += (q,)
    q = e.SetArray("A1", p)
    q = e.Offset(q, 1, 1, len(p) - 1, len(p[0]) - 1)
    for i in range(0, 10):
        q = e.CompactUp(q, False, '=($B2=' + str(i) + ')')
        time.sleep(0.5)
        printprogress('CompactUp', q)
    q = e.SetArray("A1", p)
    q = e.Offset(q, 1, 1, len(p) - 1, len(p[0]) - 1)
    for j in range(0, 10):
        q = e.CompactLeft(q, False, '=(B$2=' + str(j) + ')')
        time.sleep(0.5)
        printprogress('CompactLeft', q)

    printprogress('Formatting')
    e.InsertSheet('Sheet999')
    q = e.SetArray('Sheet999.B2', p)
    e.Activate('Sheet999')
    printprogress('FormatRange')
    e.FormatRange(q, '0,00', 'fr-BE', '=(MOD(B2;2)=0)', 'CELL')
    time.sleep(0.5)
    e.ClearFormats(q)
    printprogress('BorderRange')
    e.BorderRange(q, 'BU', '=(MOD(B$2;2)=0)', 'COLUMN')
    time.sleep(0.5)
    e.ClearFormats(q)
    e.BorderRange(q, 'UBH', '=(MOD($B2;2)=0)', 'ROW')
    time.sleep(0.5)
    e.ClearFormats(q)
    printprogress('AlignRange')
    e.AlignRange(q, 'TC', '=(MOD($B2;2)=0)', 'ROW')
    time.sleep(0.5)
    e.ClearFormats(q)
    printprogress('DecorateFont')
    e.DecorateFont(q, fontname = 'Heading 1', fontsize = 15, decoration = 'BIUS',
                   filterformula = '=(MOD(B2;2)=0)', filterscope = 'CELL')
    time.sleep(0.5)
    e.ClearFormats(q)
    printprogress('ColorizeRange')
    e.ColorizeRange(q, foreground = BASIC.RGB(255, 0, 0), background = BASIC.RGB(0, 255, 0),
                    filterformula = '=(MOD(B2;2)=0)', filterscope = 'CELL')

    e.CloseDocument(False)

    printprogress('CreatePivotTable')
    e = ui.CreateDocument('Calc')
    p = (('Item', 'State', 'Team', '2002', '2003', '2004'),
    ('Books', 'Michigan', 'Jean', 14788, 30222, 23490),
    ('Candy', 'Michigan', 'Jean', 26388, 15641, 32849),
    ('Pens', 'Michigan', 'Jean', 16569, 32675, 25396),
    ('Books', 'Michigan', 'Volker', 21961, 21242, 29009),
    ('Candy', 'Michigan', 'Volker', 26142, 22407, 32841),
    ('Pens', 'Michigan', 'Volker', 29149, 18320, 34429),
    ('Books', 'Ohio', 'Rebecca', 21845, 33503, 32200),
    ('Candy', 'Ohio', 'Rebecca', 23799, 23597, 23020),
    ('Pens', 'Ohio', 'Rebecca', 28328, 17930, 23303),
    ('Books', 'Ohio', 'Catherine', 22797, 31386, 39490),
    ('Candy', 'Ohio', 'Catherine', 13613, 16174, 35163),
    ('Pens', 'Ohio', 'Catherine', 17103, 32563, 35804),
    ('Books', 'Kentucky', 'Michelle', 29952, 19133, 33480),
    ('Candy', 'Kentucky', 'Michelle', 15348, 31094, 39722),
    ('Pens', 'Kentucky', 'Michelle', 24358, 27236, 27129),
    ('Books', 'Kentucky', 'Andy', 17199, 26386, 30450),
    ('Candy', 'Kentucky', 'Andy', 11628, 18232, 28953),
    ('Pens', 'Kentucky', 'Andy', 23828, 32031, 37551))
    q = e.SetArray('B3', p)
    time.sleep(0.5)
    r = e.Offset(q, 2 + e.Height(q), 0, 1)
    t = e.CreatePivotTable('PT1', q, r, '2002', 'Item', 'State', filterbutton = False)
    printprogress(q, t)
    time.sleep(0.5)
    r = e.Offset(t, 2 + e.Height(t), 0, 1)
    t = e.CreatePivotTable('PT2', q, r, ('2002', '2003;count', '2004;average'), 'Item', ('State', 'Team'), False)
    printprogress(q, t)
    time.sleep(0.5)

    e.CloseDocument(False)

    printprogress('Dispose')
    a = a.dispose()
    FSO.DeleteFolder(b)


# #####################################################################################################################
# TEST CHART                                                                                                        ###
#######################################################################################################################
def testChart():
    printprogress('TEST CHART')
    FSO = CreateScriptService("FileSystem")
    UI = CreateScriptService("UI")
    SESS = CreateScriptService("Session")
    import time
    t = 0.3
    s = time.sleep

    FSO.FileNaming = 'SYS'
    b = FSO.GetTempName()
    c = FSO.BuildPath(b, 'myChartDoc.ods')

    a = UI.CreateDocument("Calc")
    a.SaveAs(c, overwrite = True)
    assert len(a.Charts("~")) == 0
    a.Activate()

    printprogress('CreateChart')
    d = (('', 'X', 'Y'), ('A', 1, 5), ('B', 2, 6), ('C', 3, 7), ('D', 4, 8))
    h = a.SetArray("~.A1", d)
    chart = a.CreateChart("myChart", "~", h, True, True)

    printprogress('Resize')
    chart.Resize(10000, 4000, 12000, 9000)

    printprogress('ChartType')
    for g in ('column', 'net', 'pie', 'donut', 'area', 'bar', 'line', 'xy', 'bubble'):
        s(t)
        chart.Dim3D = False
        chart.Deep = False
        chart.Exploded = 0
        chart.Stacked = False
        chart.Percent = False
        chart.Filled = False
        chart.Legend = False
        chart.Title = ""
        chart.XTitle = ""
        chart.YTitle = ""
        printprogress('ChartType = ' + g)
        chart.ChartType = g
        s(t)
        chart.Title = 'This is a ' + g + ' chart'
        if g in ('bar', 'column'):
            for d3 in ("bar", "cone", "cylinder", "pyramid"):
                s(t)
                chart.Dim3D = d3
        else:
            s(t)
            chart.dim3d = True
        s(t)
        chart.Deep = True
        s(t)
        chart.Exploded = 25
        s(t)
        chart.legend = True
        s(t)
        chart.Stacked = True
        s(t)
        chart.XTitle = 'X-values'
        s(t)
        chart.YTitle = 'Y-values'
        s(t)
        chart.Percent = True
        s(t)
        chart.Filled = True

    printprogress('ExportToFile')
    chart.ChartType = "bar"
    chart.Title = chart.Title.replace('This is a', 'This is not a')
    for f in ('gif', 'jpeg', 'png', 'svg', 'tiff'):
        file = FSO.BuildPath(b, "myChartImage." + f)
        chart.ExportToFile(file, f, overwrite = True)
        printprogress('Image ' + file + ' exported')
        assert FSO.getFileLen(file) > 0

    printprogress('Dispose')
    a.CloseDocument(False)
    a = a.Dispose()
    FSO.DeleteFolder(b)


# #####################################################################################################################
# TEST DATABASE                                                                                                     ###
#######################################################################################################################
def testDatabase():
    printprogress('TEST DATABASE')
    import random as R
    import time
    FSO = CreateScriptService('FileSystem')
    FSO.filenaming = 'SYS'
    UI = CreateScriptService('UI')

    b = FSO.GetTempName()

    for db in ('HSQLDB', 'FIREBIRD'):
        printprogress(db, 'CreateBaseDocument')
        c = FSO.BuildPath(b, 'Test' + db + 'database.odb')
        a = UI.CreateBaseDocument(c, db)

        printprogress('GetDatabase')
        d = a.GetDatabase()

        printprogress('RunSql: Create a table')
        if db == 'HSQLDB':
            sql = "CREATE CACHED TABLE [EXPENSES]" + \
                  "([CATEGORY] VARCHAR(50),[VAT CODE] SMALLINT,[COMMENT] LONGVARCHAR,[EXPENSE DATE] TIMESTAMP(0)" + \
                  ",[DESCRIPTION] VARCHAR(50),[ID EXPENSE] INTEGER GENERATED BY DEFAULT AS IDENTITY(START WITH 0) NOT NULL PRIMARY KEY,[AMOUNT] DOUBLE)"
        elif db == 'FIREBIRD':
            sql = "CREATE TABLE [EXPENSES]" + \
                  "([CATEGORY] VARCHAR(50),[VAT CODE] SMALLINT,[COMMENT] VARCHAR(8000),[EXPENSE DATE] TIMESTAMP" + \
                  ",[DESCRIPTION] VARCHAR(50),[ID EXPENSE] INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,[AMOUNT] DOUBLE PRECISION)"
        d.RunSql(sql, directsql = True)
        a.RunCommand('DBViewTables')
        time.sleep(1)   # A pause is needed since LO 7.5 between these 2 commands ??
        a.RunCommand('DBRefreshTables')

        printprogress('RunSql: Store data')
        RecMax = 100
        for i in range(RecMax):
            f = ('0000' + str(i + 1))[-4:]
            sql = "INSERT INTO [EXPENSES]([CATEGORY], [VAT CODE], [COMMENT], [EXPENSE DATE], [DESCRIPTION], [AMOUNT]) VALUES(" + "'CAT" + \
                  f + "'," + str(int(R.random() * 3 + 1)) + ",'" + "COMM" + f + "'," + "'2020-12-" + \
                  str(int(R.random() * 10) + 11) + "'," + "'DESC" + f + "'," + str(int(R.random() * 10000)) +\
                  ")"
            d.RunSql(sql)

        printprogress('GetProperty')
        h = d.Properties()
        for p in h:
            if p != 'XMetaData':        # Otherwise error >= 25.2 tbc
                printprogress(p, d.GetProperty(p))

        a.Save()
        printprogress('GetRows')
        e = d.GetRows('EXPENSES', header = True)
        printprogress(len(e), 'rows', len(e[0]), 'columns')
        printprogress(e[1])

        printprogress('Close database')
        printprogress('ThisComponent', bas.ThisComponent)
        printprogress('ThisDatabaseDocument', bas.ThisDatabaseDocument)
        a.CloseDocument(False)

        printprogress('Read data from outside db')
        d = CreateScriptService('Database', c, readonly = True)
        g = d.DCount('[ID EXPENSE]', 'EXPENSES')
        assert g == RecMax
        g = d.GetRows('EXPENSES', header = True)
        print(len(g), 'rows', len(g[0]), 'columns')
        for i in range(len(g)):
            for j in range(len(g[0])):
                assert g[i][j] == e[i][j]

        d.closedatabase()

    printprogress('Calc.ImportFromDatabase')
    e = UI.CreateDocument('Calc')
    e.InsertSheet('Database')
    e.Activate('Database')
    printprogress('Import from registered db')
    f = '$Database.D1:E3'
    e.ImportFromDatabase('', 'Bibliography', f, 'biblio')
    printprogress('Import from table')
    f = '$Database.D25'
    e.ImportFromDatabase(c, '', f, 'EXPENSES')
    printprogress('Import from SQL')
    f = '$Database.D140'
    e.ImportFromDatabase(c, '', f, 'SELECT [CATEGORY], [AMOUNT] FROM [EXPENSES] ORDER BY [AMOUNT] DESC')

    e.CloseDocument(False)

    if bas.GetGuiType() != 1:
        FSO.DeleteFolder(b)  #	DoImport does not unlock source db in Windows ??


# #####################################################################################################################
# TEST DATASET                                                                                                    ###
#######################################################################################################################
def testDataset():
    printprogress('TEST DATASET')
    FSO = CreateScriptService('FileSystem')
    FSO.filenaming = 'SYS'

    #	Test files are copied to temp directory
    tempdir = FSO.GetTempName()
    QADir = (r'E:\ScriptForge\scriptforge\QA\QA Files' if bas.GetGuiType() == 1
             else '/home/jean-pierre/Documents/ScriptForge/Test/QA Files')
    workdir = FSO.BuildPath(tempdir, 'DatasetTests')
    FSO.CreateFolder(workdir)
    FSO.CopyFolder(QADir, workdir, overwrite = True)

    db = CreateScriptService('Database', FSO.BuildPath(workdir, 'FB NorthWind.odb'), '', False)
    a = db.CreateDataset('Order Details', orderby = '[UnitPrice]')
    assert a is not None

    printprogress('DefaultValues', a.DefaultValues)

    printprogress('Move in dataset')
    a.MoveFirst()
    printprogress(a.RowNumber, a.RowCount)
    a.MoveLast()
    printprogress(a.RowNumber, a.RowCount)
    a.BOF = True
    while a.MoveNext(300):
        printprogress(a.RowNumber, a.RowCount, a.Values)
    a.EOF = True
    while a.MovePrevious(300):
        printprogress(a.RowNumber, a.RowCount, a.Values)

    printprogress('GetRows')
    a.BOF = True
    c = True
    while not a.EOF:
        b = a.GetRows(header = True, maxrows = 300)
        if len(b) == 0:
            break
        if c:
            printprogress(b[0])
            c = False
        printprogress(b[1])

    printprogress('Properties')
    a.MoveFirst()
    for b in a.Properties():
        printprogress(b, a.GetProperty(b))

    printprogress('New dataset')
    aa = a.CreateDataset(filter = '[UnitPrice] < 50')
    printprogress('RowCount of new dataset', aa.RowCount)
    assert aa.RowCount <= a.RowCount
    aa.CloseDataset()
    aa = aa.Dispose()

    a.CloseDataset()
    a = a.Dispose()

    a = db.CreateDataset('SELECT * FROM [Categories]', filter = '[CategoryID] < 6', orderby = '[CategoryID] DESC')
    printprogress('ExportValueToFile')
    imgdir = FSO.BuildPath(tempdir, 'images')
    FSO.CreateFolder(imgdir)
    while a.MoveNext():
        b = a.GetValue('CategoryID')
        c = FSO.BuildPath(imgdir, 'IMG_' + str(b) + '.png')
        printprogress('Image:', c)
        a.ExportValueToFile('Picture', c)

    printprogress('Update')
    while a.MoveNext():
        a.Update(Description = 'Description of ' + str(a.getValue('CategoryID')))
        b = a.getValue('Description')
        printprogress(b)

    printprogress('Insert')
    a.MoveFirst()
    b = db.DMax('[CategoryID]', 'Categories')
    c = a.Insert('CategoryName', 'To drink', 'Description', 'Dining and wining')
    printprogress('Last', b, 'New', c)
    assert c > b

    printprogress("After insert", a.RowNumber)
    a.BOF = True
    while not a.EOF:
        if a.MoveNext():
            printprogress(a.GetValue('CategoryID'))

    printprogress('Reload')
    a.Reload(filter = '')
    printprogress('No filter', a.RowCount)
    a.Reload(filter = '[CategoryID] < 9')
    printprogress('New filter', a.Filter, a.RowCount)

    printprogress('CreateDataset')
    aa = a.CreateDataset(filter = '')
    printprogress('No filter', aa.RowCount)
    aa.CloseDataset()
    aa = a.CreateDataset(filter = '[CategoryID] < 9')
    printprogress('New filter', aa.Filter, aa.RowCount)
    aa.CloseDataset()

    printprogress('Delete')
    c = 0
    d = a.RowCount
    a.MoveFirst()
    while not a.EOF:
        a.Delete()
        c += 1
    printprogress(str(c) + ' records deleted')
    assert c == d

    a.CloseDataset()
    db.CloseDatabase()
    FSO.DeleteFolder(tempdir)

    printprogress('Transaction handling')
    #	Test files are copied to temp directory
    tempdir = FSO.GetTempName()
    QADir = (r'E:\ScriptForge\scriptforge\QA\QA Files' if bas.GetGuiType() == 1
             else '/home/jean-pierre/Documents/ScriptForge/Test/QA Files')
    workdir = FSO.BuildPath(tempdir, 'DatasetsTests')
    FSO.CreateFolder(workdir)
    FSO.CopyFolder(QADir, workdir, overwrite = True)

    db = CreateScriptService('Database', FSO.BuildPath(workdir, 'FB NorthWind.odb'), '', False)
    a = None
    db.SetTransactionMode(2)

    printprogress('Rollback')
    a = db.CreateDataset('SELECT * FROM [Categories]', filter = '[CategoryID] < 6', orderby = '[CategoryID] DESC')
    while a.MoveNext():
        a.Update('Description', 'Description of ' + str(a.GetValue('CategoryID')))
    db.Rollback()
    a.CloseDataset()
    assert db.DCount('[CategoryID]', 'Categories', "[Description] LIKE 'Description of %'") == 0

    printprogress('Commit')
    a = db.CreateDataset('SELECT * FROM [Categories]', filter = '[CategoryID] < 6', orderby = '[CategoryID] DESC')
    while a.MoveNext():
        a.Update('Description', 'Description of ' + str(a.getValue('CategoryID')))
    db.Commit()
    a.CloseDataset()
    assert db.DCount('[CategoryID]', 'Categories', "[Description] LIKE 'Description of %'") == 5

    db.SetTransactionMode()

    db.CloseDatabase()
    FSO.DeleteFolder(tempdir)


# #####################################################################################################################
# TEST DATASHEET                                                                                                    ###
#######################################################################################################################
def testDatasheet():
    import time
    printprogress('TEST DATASHEET')
    FSO = CreateScriptService('FileSystem')
    FSO.filenaming = 'SYS'
    UI = CreateScriptService('UI')

    #	Test files are copied to temp directory
    tempdir = FSO.GetTempName()
    QADir = ( r'E:\ScriptForge\scriptforge\QA\QA Files' if bas.GetGuiType() == 1
              else '/home/jean-pierre/Documents/ScriptForge/Test/QA Files' )
    workdir = FSO.BuildPath(tempdir, 'FormsTests')
    FSO.CreateFolder(workdir)
    FSO.CopyFolder(QADir, workdir, overwrite = True)

    a = UI.OpenBaseDocument(FSO.BuildPath(workdir, 'FB NorthWind.odb'), macroexecution = 1)

    def Test_Datasheet(ds = None, field = ''):
        time.sleep(1)
        ds.Activate()

        for prop in ds.Properties():
            printprogress(prop, ds.GetProperty(prop))

        printprogress('CreateMenu')
        c = ds.CreateMenu('More')
        c.AddItem('Details',
                  script = 'vnd.sun.star.script:QA._Datasheet.OpenOrderDetails?language=Basic&location=application')
        c.AddItem('Remove menu',
                  script = 'vnd.sun.star.script:QA._Datasheet.RemoveMenu?language=Basic&location=application')
        c.AddItem('Xray', script = 'vnd.sun.star.script:QA._Datasheet.XrayCurrent?language=Basic&location=application')
        c.AddItem('About', command = 'About')
        c.Dispose()

        ds.GoToCell(100000)
        for i in range(len(ds.ColumnHeaders)):
            ds.GoToCell(0, ds.ColumnHeaders[i])
            f = ds.GetValue(i + 1)
            g = ds.GetText(i + 1)
            printprogress('Value', ds.CurrentRow, ds.CurrentColumn, f)
            if g != f:
                printprogress('Text', ds.CurrentRow, ds.CurrentColumn, g)
            time.sleep(0.1)
        for i in range(len(ds.ColumnHeaders) - 1, -1, -1):
            ds.GoToCell(0, i)
            time.sleep(0.1)

        ds.GoToCell(5, field)
        for i in range(1, 11):
            if i <= ds.LastRow:
                ds.GoToCell(i + 5)
                f = ds.GetValue(field)
                g = ds.GetText(field)
                printprogress('Value', ds.CurrentRow, field, f)
                if f != g:
                    printprogress('Text', ds.CurrentRow, field, g)
                time.sleep(0.1)

        printprogress('ApplyFilter')
        ds.Filter = '[' + field + "] = 'NADA'"
        time.sleep(0.4)
        ds.Filter = ''
        printprogress('OrderBy')
        ds.OrderBy = '[' + field + '] DESC'
        time.sleep(0.4)
        ds.OrderBy = ''

        printprogress('RemoveMenu')
        ds.RemoveMenu('More')
        time.sleep(0.4)

        ds.CloseDatasheet()

    printprogress('Base.OpenTable')
    b = a.OpenTable('Orders')
    Test_Datasheet(b, 'ShipCountry')

    printprogress('Base.OpenQuery')
    b = a.OpenQuery('CrossTab')
    Test_Datasheet(b, 'All')

    a.CloseDocument(False)

    a = CreateScriptService("Database", FSO.BuildPath(workdir, 'FB NorthWind.odb'))

    printprogress('Database.OpenTable')
    b = a.OpenTable('Orders')
    Test_Datasheet(b, 'ShipCountry')

    printprogress('Database.OpenQuery')
    b = a.OpenQuery('CrossTab')
    Test_Datasheet(b, 'All')

    printprogress('Database.OpenSql')
    b = a.OpenSql('SELECT * FROM [Orders] ORDER BY [ShippedDate]')
    Test_Datasheet(b, 'ShipCountry')

    a.CloseDatabase()
    FSO.DeleteFolder(tempdir)


# #####################################################################################################################
# TEST DOCUMENT                                                                                                     ###
#######################################################################################################################
def testDocument():
    printprogress('TEST DOCUMENT')
    FSO = CreateScriptService("FileSystem")
    UI = CreateScriptService("UI")
    BASIC = CreateScriptService('Basic')
    import datetime

    b = FSO.GetTempName()
    c = FSO.BuildPath(b, 'myDocument.ods')
    dbf = FSO.BuildPath(b, 'myBase1.odb')
    printprogress('CreateDocument')
    a = UI.CreateDocument('Calc')
    printprogress('IsAlive', a.IsAlive)
    printprogress('CreateBaseDocument')
    dba = UI.CreateBaseDocument(dbf, 'firebird')
    printprogress('IsAlive', dba.IsAlive)

    printprogress('SaveAs')
    a.SaveAs(c, overwrite = True)
    dbf = dbf.replace('myBase1', 'myBase2')
    dba.SaveAs(dbf, overwrite = True)

    printprogress('DocumentProperties')
    a.Subject = 'This is the subject'
    a.Title = 'This is the title'
    a.Description = 'Line 01\nLine 02'
    a.Keywords = 'This, is, the, list, of, keywords'
    a.Save()
    a.closedocument()

    a = UI.OpenDocument(c, hidden = True)
    assert a.Subject == 'This is the subject'
    printprogress('Title', a.Title)
    printprogress('CellCount', a.DocumentProperties['CellCount'])
    # printprogress(a.XComponent)
    # z = a.GetProperty('XComponent')
    for h in ('Subject', 'Title', 'Description', 'Keywords'):
        printprogress(h, a.GetProperty(h))
    g = ()
    if a.IsCalc:
        g = a.Sheets
    for h in a.Properties():
        if h != 'XComponent':
            printprogress(h, a.GetProperty(h))

    printprogress('Activate')
    a.Activate()

    printprogress('CustomProperties')
    d = CreateScriptService('Dictionary')
    d['tToday'] = datetime.datetime(2020, 5, 10)
    d['nNow'] = BASIC.Now()
    d['tText'] = 'text'
    d['nNumber'] = 123.45
    a.CustomProperties = d
    [printprogress(key, ':', value) for key, value in a.CustomProperties.items()]
    d = d.Dispose()

    printprogress('SaveCopyAs')
    a.SaveCopyAs(FSO.BuildPath(b, 'myDocument2.ods'))
    a.Save()
    a.CloseDocument()
    dba.SaveCopyAs(dbf.replace('myBase2', 'myBase3'), overwrite = True)
    dba.CloseDocument()

    printprogress('Export/Import filters')
    a = a.Dispose()
    a = UI.CreateDocument('Draw')
    c = a.ExportFilters
    for e in c:
        printprogress('Export filter', e)
    c = a.ImportFilters
    for e in c:
        printprogress('Import filter', e)
    a.CloseDocument(False)

    printprogress('Dispose')
    a = a.dispose()
    dba = dba.dispose()
    FSO.DeleteFolder(b)

    a = UI.CreateDocument('Impress')
    a.CloseDocument()
    a.Dispose()


# #####################################################################################################################
# TEST FORM                                                                                                         ###
#######################################################################################################################
def testForm():
    printprogress('TEST FORM')
    import time, datetime
    FSO = CreateScriptService('FileSystem')
    FSO.filenaming = 'SYS'
    UI = CreateScriptService('UI')
    UI.Activate('BASICIDE')
    SESS = CreateScriptService('Session')

    #	Test files are copied to temp directory
    tempdir = FSO.GetTempName()
    QADir = ( r'E:\ScriptForge\scriptforge\QA\QA Files' if bas.GetGuiType() == 1
              else '/home/jean-pierre/Documents/ScriptForge/Test/QA Files' )
    workdir = FSO.BuildPath(tempdir, 'FormsTests')
    FSO.CreateFolder(workdir)
    FSO.CopyFolder(QADir, workdir, overwrite = True)

    printprogress('Base form documents')
    a = UI.OpenBaseDocument(FSO.BuildPath(workdir, 'FB NorthWind.odb'), macroexecution = 1)
    printprogress('FormDocuments', ', '.join(a.FormDocuments()))

    printprogress('Base form document open/close')
    form = 'Folder1/Double forms'
    aa = a.OpenFormDocument(form)
    aa.CloseDocument()
    aa = a.OpenFormDocument(form)
    printprogress('IsAlive', aa.IsAlive)
    b = aa.Forms()
    printprogress('List forms', ', '.join(b))

    printprogress('Base form')
    c = aa.Forms(0)

    printprogress('PrintOut')
    if testprinter:
        printprogress('SetPrinter')
        aa.SetPrinter(printer = printername, paperformat = 'Tabloid', orientation = 'Landscape')
        printprogress('Print to printer ' + printername)
        aa.PrintOut(copies = 2)

    c.closeformdocument()

    printprogress('Form properties')
    aa = a.OpenFormDocument( 'Subforms', False)
    printprogress('FormDocument.GetDatabase', aa.GetDatabase())
    b = aa.Forms(0)
    c = b.Subforms('SubForm')
    d = c.Subforms('SubSubForm')
    f = 'vnd.fsun.star.script:QA._Form.OnRecordChange?language=Basic&location=application'

    e = d.properties()
    printprogress('Set on-properties')
    for h in e:
        if h[0:2] == 'On':
            d.SetProperty(h, f)
    for h in e:
        printprogress(h, d.GetProperty(h))
    d.AllowDeletes = False
    d.AllowInserts = False
    d.AllowUpdates = False
    d.RecordSource = 'Order Details Copy'
    time.sleep(1)
    d.Filter = '[ProductID] > 100'
    time.sleep(1)
    d.Filter = ''
    time.sleep(1)
    d.OrderBy = '[ProductID] ASC'
    d.AllowDeletes = True
    d.AllowInserts = True
    d.AllowUpdates = True

    printprogress('Form methods')
    d.Requery()
    d.MoveLast()
    time.sleep(1)
    a = UI.GetDocument('FB NorthWind.odb')
    d.MoveFirst()
    time.sleep(1)
    d.MoveLast()
    time.sleep(1)
    d.MoveFirst()
    time.sleep(1)
    d.MoveNext(2)
    time.sleep(1)
    d.MovePrevious(2)

    b.CloseFormDocument()

    printprogress('Form control properties')
    form = 'AllTypes'
    aa = a.OpenFormDocument(form)
    b = aa.Forms(0)
    aa.Activate()
    d = list(b.Controls())
    d.sort()

    printprogress('READ properties (1)')
    for i in range(len(d)):
        e = b.Controls(d[i])
        printprogress(i, e.Name, e.ControlType)
        if len(e.Action) > 0:
            printprogress('', e.Name, ' ==> Action = ', e.Action)
        if len(e.Caption) > 0:
            printprogress('', e.Name, '==> Caption = ', e.Caption)
        if len(e.ControlSource) > 0:
            printprogress('', e.Name, '==> ControlSource = ', e.ControlSource)
        if e.ControlType == 'Button':
            printprogress('', e.Name, '==> Default = ', e.Default)
        if e.DefaultValue is not None:
            printprogress('', e.Name, '==> DefaultValue = ', e.DefaultValue)
        if e.ControlType != 'HiddenControl':
            printprogress('', e.Name, '==> Enabled = ', e.Enabled)
        if len(e.Format) > 0:
            printprogress('', e.Name, '==> Format = ', e.Format)
        if e.ListCount > 0:
            printprogress('', e.Name, '==> ListCount = ', e.ListCount)
        if e.ListIndex >= 0:
            printprogress('', e.Name, '==> ListIndex = ', e.ListIndex)
        if e.ControlType in ('ComboBox', 'ListBox'):
            printprogress('', e.Name, '==> Locked = ', e.Locked)
            printprogress("", e.Name, '==> MultiSelect = ', e.MultiSelect)
        g = e.Properties()
        for j in range(len(g)):
            if g[j][0:2] == 'On':
                h = e.GetProperty(g[j])
                if isinstance(h, str) and len(h) > 0:
                    printprogress('', e.Name, '==> ' + g[j] + " = ", h)
        if len(e.Picture) > 0:
            printprogress('', e.Name, '==> Picture = ', e.Picture)
        if SESS.HasUnoProperty(e.XControlModel, 'InputRequired'):
            printprogress('', e.Name, '==> Required = ', e.Required)
        if e.ControlType not in ('ComboBox', 'ListBox'):
            printprogress('', e.Name, '==> ListSource = ', e.ListSource)
        if e.ControlType not in ('ComboBox', 'ListBox'):
            printprogress('', e.Name, '==> ListSourceType = ', e.ListSourceType)
        if e.ControlType in ('ComboBox', 'DateField', 'FileControl', 'FormattedField', 
                             'PatternField', 'textField', 'TimeField'):
            printprogress('', e.Name, '==> Text = ', e.Text)
        if len(e.TipText) > 0:
            printprogress('', e.Name, '==> TipText = ', e.TipText)
        if e.ControlType == 'CheckBox':
            printprogress('', e.Name, '==> TripleState = ', e.TripleState)
        printprogress('', e.Name, '==> Value = ', e.Value)

    printprogress('MODIFY properties (1)')
    b.Controls('Push Button 1').Action = 'movetonew'
    b.Controls('Group Box 1').Caption = 'New Caption !!'
    b.Controls('Push Button 1').Default = True
    b.Controls('Text Box 1').Enabled = False
    b.Controls('Time Field 1').Format = '24h short'
    b.Controls('Combo Box 1').ListIndex = 10
    b.Controls('Combo Box 1').Locked = True
    b.Controls('List Box 1').MultiSelect = True
    b.Controls(
        'Text Box 1').OnMouseEntered = 'vnd.sun.star.script:QA._Form.OnControlEvent?language=Basic&location=application'
    b.Controls('Image Control 1').Picture = \
        '/home/jean-pierre/Documents/Access2Base/Doc/Access2Base/_wikiimages/tracelog dialog.png'
    b.Controls('Formatted Field 1').Required = True
    from com.sun.star.form.ListSourceType import TABLEFIELDS
    b.Controls('Combo Box 1').ListSourceType = TABLEFIELDS
    b.Controls('Combo Box 1').Locked = False
    b.Controls('Combo Box 1').ListSource = 'Products'
    b.Controls('Text Box 1').TipText = 'Special text here'
    b.Controls('Check Box 1').TripleState = True
    for i in range(5):
        b.Controls('List Box 1').Visible = False
        time.sleep(0.15)
        b.Controls('List Box 1').Visible = True
        time.sleep(0.15)

    printprogress('MODIFY Value property (1)')
    b.Controls('Check Box 1').Value = True
    b.Controls('Combo Box 1').Value = 'UnitsInStock'
    b.Controls('Currency Field 1').Value = 123.45
    b.Controls('Date Field 1').Value = datetime.datetime(2021, 1, 30)
    b.Controls('File Selection 1').Value = \
        '/home/jean-pierre/Documents/Access2Base/Doc/Access2Base/_wikiimages/tracelog dialog.png'
    b.Controls('Fixed Text 1').Caption = 'Other text'
    b.Controls('Formatted Field 1').Value = 'ABCD'
    b.Controls('List Box 1').MultiSelect = False
    b.Controls('List Box 1').Value = '92'
    b.Controls('Numeric Field 1').Value = b.Controls('Numeric Field 1').Value + 1
    b.Controls('Option Button 1').Value = True
    b.Controls('Option Button 2').Value = True
    b.Controls('Pattern Field 1').Value = 'VWXYZ'
    b.Controls('Scrollbar 1').Value = 50
    b.Controls('Spin Button 1').Value = 75
    b.Controls('Text Box 1').Value = 'Text in main text box'
    b.Controls('Time Field 1').Value = bas.Now()

    printprogress("Table control properties")
    c = b.Controls("Table Control  1")    #	2 spaces !?
    d = list(c.Controls())
    d.sort()

    printprogress('READ properties (2)', c.Name)
    for i in range(len(d)):
        e = c.Controls(d[i])
        printprogress(i, e.Name, e.ControlType)
        if len(e.Action) > 0:
            printprogress('', e.Name, ' ==> Action = ', e.Action)
        if len(e.Caption) > 0:
            printprogress('', e.Name, '==> Caption = ', e.Caption)
        if len(e.ControlSource) > 0:
            printprogress('', e.Name, '==> ControlSource = ', e.ControlSource)
        if e.ControlType == 'Button':
            printprogress('', e.Name, '==> Default = ', e.Default)
        if e.DefaultValue is not None:
            printprogress('', e.Name, '==> DefaultValue = ', e.DefaultValue)
        if e.ControlType != 'HiddenControl':
            printprogress('', e.Name, '==> Enabled = ', e.Enabled)
        if len(e.Format) > 0:
            printprogress('', e.Name, '==> Format = ', e.Format)
        if e.ListCount > 0:
            printprogress('', e.Name, '==> ListCount = ', e.ListCount)
        if e.ListIndex >= 0:
            printprogress('', e.Name, '==> ListIndex = ', e.ListIndex)
        if e.ControlType in ('ComboBox', 'ListBox'):
            printprogress('', e.Name, '==> Locked = ', e.Locked)
            printprogress("", e.Name, '==> MultiSelect = ', e.MultiSelect)
        g = e.Properties()
        for j in range(len(g)):
            if g[j][0:2] == 'On':
                h = e.GetProperty(g[j])
            if isinstance(h, str) and len(h) > 0:
                printprogress('', e.Name, '==> ' + g[j] + " = ", h)
        if len(e.Picture) > 0:
            printprogress('', e.Name, '==> Picture = ', e.Picture)
        if SESS.HasUnoProperty(e.XControlModel, 'InputRequired'):
            printprogress('', e.Name, '==> Required = ', e.Required)
        if e.ControlType in ('ComboBox', 'ListBox'):
            printprogress('', e.Name, '==> ListSource = ', e.ListSource)
        if e.ControlType in ('ComboBox', 'ListBox'):
            printprogress('', e.Name, '==> ListSourceType = ', e.ListSourceType)
        if e.ControlType in ('ComboBox', 'DateField', 'FileControl', 'FormattedField',
                             'PatternField', 'TextField', 'TimeField'):
            printprogress('', e.Name, '==> Text = ', e.Text)
        if len(e.TipText) > 0:
            printprogress('', e.Name, '==> TipText = ', e.TipText)
        if e.ControlType == 'CheckBox':
            printprogress('', e.Name, '==> TripleState = ', e.TripleState)
        printprogress('', e.Name, '==> Value = ', e.Value)

    printprogress('MODIFY properties (2)')
    c.Controls('Text Box 1').Enabled = False
    c.Controls('Time Field 1').Format = '24h short'
    c.Controls('Combo Box 1').ListIndex = 2
    c.Controls('Combo Box 1').Locked = True
    c.Controls('List Box 1').MultiSelect = True
    c.Controls(
        'Text Box 1').OnMouseEntered = 'vnd.sun.star.script:QA._Form.OnControlEvent?language=Basic&location=application'
    from com.sun.star.form.ListSourceType import TABLEFIELDS
    c.Controls('Combo Box 1').ListSourceType = TABLEFIELDS
    c.Controls('Combo Box 1').Locked = False
    c.Controls('Combo Box 1').ListSource = 'Products'
    c.Controls('Text Box 1').TipText = 'Special text here'
    c.Controls('Check Box 1').TripleState = True

    printprogress('MODIFY Value property (2)')
    c.Controls('Check Box 1').Value = True
    c.Controls('Combo Box 1').Value = 'UnitsInStock'
    c.Controls('Currency Field 1').Value = 123.45
    c.Controls('Date Field 1').Value = datetime.datetime(2021, 1, 30)
    c.Controls('List Box 1').MultiSelect = False
    c.Controls('Numeric Field 1').Value = c.Controls('Numeric Field 1').Value + 1
    c.Controls('Pattern Field 1').Value = 'VWXYZ'
    c.Controls('Text Box 1').Value = 'Text in table text box'
    c.Controls('Time Field 1').Value = bas.Now()

    e = c.Controls('Text Box 1')
    printprogress('', e.Name, '==> Value = ', e.Value)

    printprogress('SetFocus')
    c.Controls('Time Field 1').SetFocus()
    time.sleep(1)

    x = b.XForm
    # x.cancelRowUpdates()
    # b.CloseFormDocument()

    a.CloseDocument(False)
    FSO.DeleteFolder(tempdir)


# #####################################################################################################################
# TEST WRITER                                                                                                       ###
#######################################################################################################################
def testWriter():
    printprogress('TEST WRITER')
    FSO = CreateScriptService('FileSystem')
    FSO.filenaming = 'SYS'
    UI = CreateScriptService('UI')

    b = FSO.GetTempName()
    g = FSO.BuildPath(b, 'myDocumentW.odt')

    printprogress('CreateDocument')
    a = ui.CreateDocument('', FSO.BuildPath(FSO.TemplatesFolder, 'personal/CV.ott'))
    printprogress('IsAlive', a.IsAlive)

    printprogress('SaveAs')
    a.SaveAs(g, overwrite = True)

    printprogress('Export/Import filters')
    q = a.ExportFilters
    for e in q:
        printprogress('Export filter', e)
    q = a.ImportFilters
    for e in q:
        printprogress('Import filter', e)

    printprogress('PrintOut')
    if testprinter:
        printprogress('SetPrinter')
        a.SetPrinter(printer = printername, paperformat = 'Tabloid', orientation = 'Landscape')
        printprogress('Print to printer ' + printername)
        a.PrintOut(copies = 2)

    printprogress('Export to PDF')
    d = FSO.BuildPath(b, "myDocumentW.pdf")
    a.ExportAsPDF(d, password = 'xxx', watermark = 'Secret')
    assert FSO.FileExists(d)

    printprogress('Context menus')
    g = a.ContextMenus()
    print(g)
    g = a.ContextMenus('text', submenuchar = '>')
    g.Activate(False)
    s = 'vnd.sun.star.script:Standard.XContextMenu.MyFunction?language=Basic&location=document'
    g.AddItem('Item A', command = 'About')
    g.AddItem('Item B>Item B.1', script = s)
    g.AddItem('Item B>Item B.2', script = s)
    g.AddItem('---')
    g.AddItem('Item C>Item C.1>Item C.1.1', script = s)
    g.AddItem('Item C>Item C.1>Item C.1.2', script = s)
    g.AddItem('Item C>---')
    g.AddItem('Item C>Item C.2>Item C.2.1', script = s)
    g.AddItem('Item C>Item C.2>Item C.2.2', script = s)
    g.Activate()
    g.Dispose()

    printprogress('RunCommand')
    a.RunCommand('BasicIDEAppear', Document = 'My Macros & Dialogs', LibName = 'QA', Name = '_Session', Line = 92)

    a.CloseDocument(False)
    printprogress('Styles')
    a = ui.CreateDocument('Writer')
    c = a.StyleFamilies
    for h in c:
        printprogress('Style family :', h)
    printprogress('XStyle', a.XStyle('ParagraphStyles', 'Default Paragraph Style'))
    d = FSO.BuildPath(FSO.TemplatesFolder, 'wizard/agenda/aw-9colorful.ott')
    printprogress("# of styles", len(a.Styles("ParagraphStyles")))
    a.ImportStylesFromFile(d, 'CharacterStyles', overwrite = True)
    printprogress("# of styles", len(a.Styles("ParagraphStyles")))
    a.ImportStylesFromFile(d, overwrite = True) # All families
    printprogress("# of styles", len(a.Styles('ParagraphStyles')))
    e = a.Styles('ParagraphStyles', 'Ag*', used = False)
    for h in e:
        printprogress('ParagraphStyles', 'Ag*', h)
    printprogress('DeleteStyles')
    a.DeleteStyles('ParagraphStyles', e)
    printprogress("# of styles", len(a.Styles('ParagraphStyles')))

    printprogress('Dispose')
    a.CloseDocument(False)
    a = a.Dispose()
    FSO.DeleteFolder(b)


# #####################################################################################################################
# TEST DIALOG & DIALOGCONTROL                                                                                       ###
#######################################################################################################################
def testDialog():
    printprogress('TEST DIALOG & DIALOGCONTROL')
    import time, datetime, uno
    FSO = CreateScriptService('FileSystem')
    FSO.filenaming = 'SYS'
    UI = CreateScriptService('UI')
    # UI.Activate('BASICIDE')
    BAS = CreateScriptService('Basic')
    
    a = CreateScriptService('Dialog', container = 'GlobalScope', library = 'QA', dialogname = 'QADialog')
    b = a.Execute(False)    #	Non-modal mode

    printprogress('TreeControl')
    c = a.Controls('TreeControl1')
    printprogress(c.XControlView)
    d = c.CreateRoot('ROOT 1')
    db = CreateScriptService('Database', '', 'Bibliography')
    e = 'SELECT [Custom1], [Author], [Title] FROM [biblio] ' + \
        'WHERE [Author] IS NOT NULL ORDER BY [Custom1], [Author], [Title]'
    f = db.GetRows(e)
    db.CloseDatabase()
    c.AddSubTree(d, f)
    c.XControlView.expandNode(d)
    
    printprogress('TreeControl/CurrentNode')
    c.CurrentNode = d

    printprogress('TreeControl/FindNode')
    c.CurrentNode = c.FindNode('*Sophie*', casesensitive = False)

    printprogress('Dialog Properties Get')
    for d in a.Properties():
        printprogress(a.Name, d, a.GetProperty(d))

    printprogress('Dialog Properties Setprintprogress')
    a.Caption = 'This is the title'
    printprogress(a.Name, 'Caption', a.Caption)
    he = a.Height
    a.Height = 400
    a.SetProperty('Height', 500)
    printprogress(a.Name, 'Height', a.Height)
    wi = a.Width
    a.Width = 500
    a.SetProperty('Width', 800)
    printprogress(a.Name, 'Width', a.Width)
    a.Height = he
    a.Width = wi
    a.Page = 2
    a.SetProperty('Page', 3)
    printprogress(a.Name, 'Page', a.Page)
    a.Page = 0
    a.Visible = False
    a.SetProperty('Visible', False)
    printprogress(a.Name, 'Visible', a.Visible)
    a.Visible = True
    printprogress(a.Name, 'OnMouseEntered', a.OnMouseEntered)

    printprogress('Tree control select & expand events')
    c = a.Controls('TreeControl1')
    c.OnNodeSelected = 'vnd.sun.star.script:QA._Dialog.NodeSelectedEvent?language=Basic&location=application'
    c.OnNodeExpanded = 'vnd.sun.star.script:QA._Dialog.NodeExpandedEvent?language=Basic&location=application'

    printprogress('Activate')
    ui.Activate('BASICIDE')
    a.Activate()

    printprogress('Controls')
    c = a.Controls()
    for i in range(len(c)):
        printprogress('Control #' + str(i), c[i])
    printprogress('Controls Properties Get')
    for d in c:
        e = a.Controls(d)
        h = e.Properties()
        for f in h:
            if f not in ('CurrentNode', 'RootNode'):    # because they return UNO objects
                printprogress(e.Name, f, e.GetProperty(f))

    printprogress('Controls Properties Set')

    def setprops(val):
        for d in c:
            e = a.Controls(d)
            if e.ControlType in g or len(g) == 0:
                e.SetProperty(f, val)
                printprogress(e.Name, f, e.GetProperty(f))

    f = 'Cancel'
    g = ('Button',)
    setprops(True)
    
    f = 'Caption'
    g = ('Button', 'CheckBox', 'FixedLine', 'FixedText', 'GroupBox', 'RadioButton')
    setprops('New Label')
    
    f = 'Default'
    g = ('Button',)
    setprops(True)
    
    f = 'Enabled'
    g = ()
    setprops(False)
    setprops(True)
    
    f = 'Format'
    g = ('DateField',)
    setprops('MM/DD/YYYY')
    g = ('TimeField',)
    setprops('24h long')

    f = 'ListIndex'
    g = ('ComboBox', 'ListBox', 'TableControl')
    for d in c:
        e = a.Controls(d)
        if e.ControlType in g or len(g) == 0:
            e.SetProperty(f, e.ListCount - 1)   # Select last
            printprogress(e.Name, f, e.GetProperty(f))

    f = 'Locked'
    g = ('ComboBox', 'CurrencyField', 'DateField', 'FileControl', 'FormattedField', 'ListBox',
                               'NumericField', 'PatternField', 'TextField', 'TimeField')
    setprops(True)
    setprops(False)

    f = 'MultiSelect'
    g = ('ListBox')
    setprops(True)

    f = 'OnKeyPressed'
    g = ()
    h = a.Controls('CommandButton1').OnActionPerformed
    setprops(h)
    setprops('')

    f = 'RowSource'
    g = ('ComboBox', 'ListBox')
    setprops(('MNO', 'PQR', 'STU', 'VWX', 'YZABC'))

    f = 'TipText'
    g = ()
    setprops('New tiptext !!')

    f = 'TripleState'
    g = ('CheckBox')
    setprops(True)

    f = 'Value'
    g = ('Button', 'CheckBox', 'ComboBox', 'CurrencyField', 'DateField', 'FileControl',
         'FormattedField', 'ListBox', 'NumericField', 'PatternField', 'ProgressBar', 'RadioButton', 'ScrollBar',
         'TextField', 'TimeField')
    for d in c:
        e = a.Controls(d)
        h = e.ControlType
        if h in g or len(g) == 0:
            if h in ('Button', 'CheckBox', 'RadioButton'):
                e.SetProperty(f, True)
            elif h in ('ComboBox', 'FileControl', 'PatternField', 'TextField'):
                e.SetProperty(f, 'YZABC')
            elif h in ('CurrencyField', 'FormattedField', 'NumericField', 'ProgressBar', 'ScrollBar'):
                e.SetProperty(f, 50)
            elif h in ('DateField', 'TimeField'):
                e.SetProperty(f, BAS.Now())
            elif h == 'ListBox':
                e.SetProperty(f, ('MNO', 'VWX'))
            printprogress(e.Name, f, e.GetProperty(f))

    f = 'Visible'
    g = ()
    setprops(False)
    setprops(True)

    printprogress('TableControl')
    e = a.Controls("GridControl1")
    printprogress(e.Value)
    f = (('', 'Col1', 'Col2', 'Col3'),)
    f += (("A", 1, 2, 3),)
    e.SetTableData(f, widths = (1.5, 4.0), alignments = 'C')
    e.ListIndex = 0
    printprogress(e.Value)
    time.sleep(1)
    f += (("B", 4, 5, 6),)
    e.SetTableData(f, widths = (4, 1.5, 4.0), alignments = ' C')
    e.ListIndex = 1
    printprogress(e.Value)
    time.sleep(1)
    f += (("C", 7, 8, 9),)
    e.SetTableData(f, widths = (4, 4, 1.5), alignments = '  C')
    e.ListIndex = 2
    printprogress(e.Value)
    assert e.Value[2] == 9
    time.sleep(1)

    printprogress('Resize / Center')
    a.Center()
    time.sleep(1)
    a.Resize(height=300)
    time.sleep(1)
    a.Center()
    time.sleep(1)
    p = CreateScriptService('Dialog', 'GlobalScope', 'ScriptForge', 'dlgProgress')
    p.Center(a)
    p.Execute(False)
    time.sleep(1)
    a.Resize()
    time.sleep(1)
    a.Center()
    p.Center(a)
    time.sleep(1)
    p.Terminate()

    printprogress('Editable On-properties')
    b = a.Controls('ListBox1')
    s = 'vnd.sun.star.script:QA._Dialog.Dialog_Control_Event?language=Basic&location=application'
    d = a.Controls('ScrollBar1')
    e = a.Controls('CommandButton1')
    f = a.Controls('CheckBox1')
    g = a.Controls('ComboBox1')
    e.OnActionPerformed = s
    d.OnAdjustmentValueChanged = s
    b.OnFocusGained = s
    b.OnFocusLost = s
    b.OnItemStateChanged = s
    b.OnKeyPressed = s
    b.OnKeyReleased = s
    b.OnMouseDragged = s
    b.OnMouseEntered = s
    b.OnMouseExited = s
    b.OnMouseMoved = s
    b.OnMousePressed = s
    b.OnMouseReleased = s
    g.OnTextChanged = s
    time.sleep(10)

    printprogress('Resize controls')
    b = a.Controls()
    for c in b:
        d = a.Controls(c)
        d.Resize(100, 100, 500, 200)
        printprogress(c, d.X, d.Y, d.Width, d.Height)
        time.sleep(0.5)
        d.Resize()
    printprogress('X, Y, Width, Height')
    b = a.Controls()
    for c in b:
        d = a.Controls(c)
        d.X = 100
        d.Y = 300
        d.Width = 500
        d.Height = 200
        printprogress(c, d.X, d.Y, d.Width, d.Height)
        time.sleep(0.5)
        d.Resize()

    a.Terminate()

    printprogress("SetPageManager")
    a = CreateScriptService("Dialog", "GlobalScope", "QA", "QAPages")
    a.SetPageManager("PageList,PageCombo,PageRadio1", "PageButton1,PageButton2,PageButton3,PageButton4",
                     "BackButton,NextButton", lastpage = 4)
    a.Execute(False)
    for i in range(1, 5):
        a.Page = i
        time.sleep(1)
    for i in range(4, 0, -1):
        a.Page = i
        time.sleep(1)
    a.Terminate()
    a = a.Dispose()

    printprogress('CREATE NEW DIALOG')
    # a = CreateScriptService('dialog', 'GlobalScope', 'QA', 'QAResize')
    a = CreateScriptService('SFDialogs.newdialog', 'myDialog', place = (100, 100, 300, 210))
    a.Execute(False)
    # BAS.xray(a.XDialogView)

    sleep = 0.5

    printprogress('Create new BUTTON')
    b = a.CreateButton('NEWBUTTON', (150, 120, 100, 20), False, 'OK')
    b.Caption = 'New button xxxxxx' + '\n' + 'yyyyyyyyyy zzzzzzzzzz'
    b.Resize()
    # b.OnActionPerformed = 'vnd.sun.star.script: QA.Unit_Tests.KeyPressed?language = Basic + location = application'
    b.Value = True
    q = a.CloneControl(b.Name, b.Name + '_DUP', b.X + 10, b.Y + 10)
    time.sleep(sleep)
    b.Visible = False
    q.Visible = False

    printprogress('Create new CHECKBOX')
    b = a.CreateCheckBox('NEWCHECK', (150, 150, 100, 40), multiline = True)
    b.Caption = 'New checkbox xxxxxx' + '\n' + 'yyyyyyyyyy zzzzzzzzzz'
    b.Resize()
    b.TripleState = True
    b.Value = True
    b.XControlModel.TextColor = BAS.RGB(255, 0, 0)
    q = a.CloneControl(b.Name, b.Name + '_DUP', b.X + 10, b.Y + 10)
    time.sleep(sleep)
    b.Visible = False
    q.Visible = False

    printprogress('Create new COMBOBOX')
    b = a.CreateComboBox('NEWCOMBO', (150, 30, 100, 100), dropdown = True, linecount = 5)
    b.RowSource = ('New combobox', 'abcd', 'defg', 'xyz', '123', "'(§", '789', '101112')
    b.Resize()
    b.Value = 'xyz'
    b.Border = 'none'  # OK
    q = a.CloneControl(b.Name, b.Name + '_DUP', b.X + 10, b.Y + 10)
    time.sleep(sleep)
    b.Visible = False
    q.Visible = False

    printprogress('Create new CURRENCYFIELD')
    b = a.CreateCurrencyField('NEWCURENCY', (150, 30, 100, 100), minvalue = 0, maxvalue = 1000, increment = 100,
                              spinbutton = True, accuracy = 3)
    # b.Resize()
    b.Border = 'none'  # Effect on spin button only
    b.Value = 100.50
    b.Locked = True
    q = a.CloneControl(b.Name, b.Name + '_DUP', b.X + 10, b.Y + 10)
    time.sleep(sleep)
    b.Visible = False
    q.Visible = False

    printprogress('Create new DATEFIELD')
    b = a.CreateDateField('NEWDATE', (150, 30, 100, 20), 'none', mindate = datetime.datetime(2020, 1, 1, 0, 0, 0, 0),
                          maxdate = datetime.datetime(2023, 12, 31, 0, 0, 0, 0), dropdown = True)
    b.Value = BAS.Now()
    b.Format = 'Standard (long)'
    b.Border = '3D'  # No effect
    b.Resize()
    b.Locked = True
    q = a.CloneControl(b.Name, b.Name + '_DUP', b.X + 10, b.Y + 10)
    time.sleep(sleep)
    b.Visible = False
    q.Visible = False

    printprogress('Create new FILECONTROL')
    b = a.CreateFileControl('NEWFILE', (15, 180, 300, 20), 'NONE')
    b.Border = '3D'  # No effect
    b.Value = '/home/jean-pierre/Documents/Access2Base/Doc/Basic'
    b.Resize()
    q = a.CloneControl(b.Name, b.Name + '_DUP', b.X + 10, b.Y + 10)
    time.sleep(sleep)
    b.Visible = False
    q.Visible = False

    printprogress('Create new FIXEDLINE')
    b = a.CreateFixedLine('NEWLINE', (15, 100, 120, 20), orientation = 'h')
    b.Caption = 'Line'
    b.XControlModel.TextColor = BAS.RGB(255, 0, 0)
    b.XControlModel.FontRelief = 1
    q = a.CloneControl(b.Name, b.Name + '_DUP', b.X + 10, b.Y + 10)
    time.sleep(sleep)
    b.Visible = False
    q.Visible = False

    printprogress('Create new FIXEDTEXT')
    b = a.CreateFixedText('NEWFIXTEXT', (15, 120, 120, 80), '3d', multiline = True, align = 'Right',
                          verticalalign = 'middle')
    b.Border = 'none'  # No effect
    b.Caption = 'Line1' + '\n' + 'Line2' + '\n' + '\n' + 'Line4'
    q = a.CloneControl(b.Name, b.Name + '_DUP', b.X + 10, b.Y + 10)
    time.sleep(sleep)
    b.Visible = False
    q.Visible = False

    printprogress('Create new FORMATTEDFIELD')
    b = a.CreateFormattedField('NEWFORMATTED', (15, 120, 120, 80), spinbutton = True, border = '3d')
    b.Value = 1234.56
    b.Format = '#.000,00E+00'
    b.Resize()
    q = a.CloneControl(b.Name, b.Name + '_DUP', b.X + 10, b.Y + 10)
    time.sleep(sleep)
    b.Visible = False
    q.Visible = False

    printprogress('Create new GROUPBOX')
    b = a.CreateGroupBox('NEWGROUP', (15, 120, 120, 80))
    b.Caption = 'Group box'
    q = a.CloneControl(b.Name, b.Name + '_DUP', b.X + 10, b.Y + 10)
    time.sleep(sleep)
    b.Visible = False
    q.Visible = False

    printprogress('Create new HYPERLINK')
    b = a.CreateHyperlink('NEWHYPERLINK', (15, 120, 120, 80), '3d', multiline = True, align = 'center',
                          verticalalign = 'middle')
    b.Border = 'none'  # No effect
    b.Caption = 'This is' + '\n' + 'clickable !'
    b.URL = 'http://www.access2base.com'
    q = a.CloneControl(b.Name, b.Name + '_DUP', b.X + 10, b.Y + 10)
    time.sleep(sleep)
    b.Visible = False
    q.Visible = False

    printprogress('Create new IMAGECONTROL')
    b = a.CreateImageControl('NEWIAMGE', (150, 15, 130, 250), '3d', 'FITTOSIZE')
    b.Picture = '/home/jean-pierre/Downloads/Graph.png'
    # b.Resize()
    q = a.CloneControl(b.Name, b.Name + '_DUP', b.X + 10, b.Y + 10)
    time.sleep(sleep)
    b.Visible = False
    q.Visible = False

    printprogress('Create new LISTBOX')
    b = a.CreateListBox('NEWLIST', (150, 30, 100, 100), dropdown = True, linecount = 5, multiselect = True)
    b.RowSource = ('New listbox', 'abcd', 'defg', 'xyz', '123', "'(§", '789', '101112')
    b.Resize()
    b.Value = ('xyz', 'abcd')
    b.Border = 'none'  # No effect
    q = a.CloneControl(b.Name, b.Name + '_DUP', b.X + 10, b.Y + 10)
    time.sleep(sleep)
    b.Visible = False
    q.Visible = False

    printprogress('Create new NUMERICFIELD')
    b = a.CreateNumericField('NEWNUMERIC', (150, 30, 150, 32), minvalue = 0, maxvalue = 1000, increment = 100,
                             spinbutton = True, accuracy = 3)
    b.Border = '3d'  # Effect on spin button only
    b.Value = 1000
    b.Resize()
    b.Locked = False
    b.Value = 995
    q = a.CloneControl(b.Name, b.Name + '_DUP', b.X + 10, b.Y + 10)
    time.sleep(sleep)
    b.Visible = False
    q.Visible = False

    printprogress('Create new PATTERNFIELD')
    b = a.CreatePatternField('NEWPATTERN', (150, 30, 150, 32), 'none', editmask = 'NNLNNLLLLL',
                             literalmask = '__/__/2023')
    b.Value = '31122023'
    b.Resize()
    q = a.CloneControl(b.Name, b.Name + '_DUP', b.X + 10, b.Y + 10)
    time.sleep(sleep)
    b.Visible = False
    q.Visible = False

    printprogress('Create new PROGRESSBAR')
    b = a.CreateProgressBar('NEWPROGRESS', (150, 30, 150, 32), 'none', minvalue = 0, maxvalue = 200)
    b.Value = 75
    q = a.CloneControl(b.Name, b.Name + '_DUP', b.X + 10, b.Y + 10)
    time.sleep(sleep)
    b.Visible = False
    q.Visible = False

    printprogress('Create new RADIOBUTTON')
    b = a.CreateRadioButton('NEWRADIO', (150, 120, 100, 40), multiline = True)
    b.Caption = 'New radio button xxxxxx' + '\n' + 'yyyyyyyyyy zzzzzzzzzz'
    b.Resize()
    b.Value = True
    b.XControlModel.TextColor = bas.RGB(255, 0, 0)
    q = a.CloneControl(b.Name, b.Name + '_DUP', b.X + 10, b.Y + 10)
    time.sleep(sleep)
    b.Visible = False
    q.Visible = False

    printprogress('Create new SCROLLBAR')
    b = a.CreateScrollBar('NEWSCROLL', (150, 30, 130, 8), orientation = 'H', border = '3d', minvalue = 0,
                          maxvalue = 200)
    b.Value = 75
    q = a.CloneControl(b.Name, b.Name + '_DUP', b.X + 10, b.Y + 10)
    time.sleep(sleep)
    b.Visible = False
    q.Visible = False

    printprogress('Create new TABLECONTROL')
    b = a.CreateTableControl('NEWSGRID', (150, 0, 130, 150), scrollbars = 'N', border = '3d', rowheaders = True,
                             columnheaders = True, gridlines = True)
    c = [('Column A', 'Column B', 'Column C')]
    for i in range(41):
        c.append(('Row ' + str(i), i * 2 + 1, i * 2 + 2))
    b.SetTableData(c, (1, 1.5), 'CC', rowheaderwidth = 25)
    q = a.CloneControl(b.Name, b.Name + '_DUP', b.X + 10, b.Y + 10)
    time.sleep(sleep)
    b.Visible = False
    q.Visible = False

    printprogress('Create new TEXTFIELD')
    b = a.CreateTextField('NEWTEXT', (15, 120, 120, 40), '3d', multiline = True, maximumlength = 0,
                          passwordcharacter = '')
    b.Value = 'New text field xxxxxx' + '\n' + 'yyyyyyyyyy zzzzzzzzzz'
    # b.Resize()
    q = a.CloneControl(b.Name, b.Name + '_DUP', b.X + 10, b.Y + 10)
    time.sleep(sleep)
    b.Visible = False
    q.Visible = False

    printprogress('Create new TIMEFIELD')
    b = a.CreateTimeField('NEWTIME', (15, 120, 120, 40), 'none', mintime = datetime.datetime(1899, 12, 30, 1, 0, 0, 0))
    b.Value = datetime.datetime(1899, 12, 30, 13, 15, 40, 0)
    b.Resize()
    q = a.CloneControl(b.Name, b.Name + '_DUP', b.X + 10, b.Y + 10)
    time.sleep(sleep)
    b.Visible = False
    q.Visible = False

    printprogress('Create new TREECONTROL')
    b = a.CreateTreeControl('NEWTREE', (150, 10, 130, 140), '3D')
    d = b.CreateRoot('Biblio')
    db = CreateScriptService('Database', '', 'Bibliography')
    sql = 'SELECT [Custom1], [Author], [Title] FROM [biblio] WHERE [Author] IS NOT NULL ORDER BY [Custom1], [Author], [Title]'
    f = db.GetRows(sql)
    db.CloseDatabase()
    b.AddSubTree(d, f)
    b.XControlView.expandNode(d)
    q = a.CloneControl(b.Name, b.Name + '_DUP', b.X + 10, b.Y + 10)
    time.sleep(sleep)
    b.Visible = False
    q.Visible = False

    a.Terminate()
    a = a.Dispose()

    printprogress('OrderTabs')
    a = CreateScriptService('Dialog', 'GlobalScope', 'QA', 'QADialog')
    f = a.OrderTabs(('CurrencyField1', 'TreeControl1', 'FixedLine1', 'CommandButton1', 'TimeField1'), 10, 10)
    c = a.Controls()
    for d in c:
        e = a.Controls(d)
        printprogress(e.Name, e.TabIndex)
    # a.Execute()

    a.Terminate()
    a = a.Dispose()


#####################################################################################################################
# TEST TOOLBAR & TOOLBARBUTTON                                                                                     ###
#######################################################################################################################
def testToolbar():
    printprogress('TEST SF_TOOLBAR')
    ui = CreateScriptService('UI')
    FSO = CreateScriptService('FileSystem')
    FSO.FileNaming = 'ANY'
    basic = CreateScriptService('Basic')

    b = '/home/jean-pierre/Documents/ScriptForge/Test/Tests/Test Duplicates.ods'
    bOpen = (basic.ConvertToUrl(b) in ui.Documents())
    if bOpen:
        a = ui.GetDocument(b)
    else:
        a = ui.OpenDocument(b)

    printprogress('List all properties')
    c = a.Toolbars()
    for d in c:
        d1 = a.Toolbars(d)
        for e in d1.Properties():
            printprogress(d1.Name, e, d1.GetProperty(e))
        if d1.Visible:
            f = d1.ToolbarButtons()
            for e in f:
                f1 = d1.ToolbarButtons(e)
                for g in f1.Properties():
                    printprogress(d1.Name, f1.Caption, g, f1.GetProperty(g))

    printprogress('Execute ...')
    c = a.Toolbars('Test')
    d = c.ToolbarButtons('Execute Button')
    e = d.Execute()

    if not bOpen:
        a.CloseDocument(False)


def ControlEvent(poEvent):
    try:
        a = CreateScriptService("SFDialogs.DialogEvent", poEvent)
        b = 10  # Remove the "1" to simulate an event error
        c = 10 / b
        if a is not None:
            exc.DebugPrint('Event triggered from Python in control', a.Name, 'of dialog', a.Parent.Name)
    except Exception as e:
        exc.DebugDisplay(type(e), traceback.format_exc())


def OpenDialog(event=None):
    my_dialog = CreateScriptService("Dialog", "GlobalScope", "QA", "QADialog")
    exc.PythonShell({**globals(), **locals()})
    print(my_dialog)
    my_dialog.Center()
    my_dialog.Execute()
    my_dialog.Terminate()

def CloseDialog(poEvent):
    a = CreateScriptService("SFDialogs.DialogEvent", poEvent)
    exc.PythonShell()
    if a is not None:
        exc.DebugPrint('Event triggered from Python in control', a.Name, 'of dialog', a.Parent.Name)
        b = a.Parent
        if b is not None:
            exc.DebugPrint(b.Name, b.Controls())
            b.Center()
            b.EndExecute(0)


def TestMenu():
    #   Invoked to test the Menu service autonomously

    a = ui.CreateDocument("Writer")
    b = a.CreateMenu("My Menu", "", " > ")
    s = "vnd.sun.star.script:QA._Menu.MenuMsg?language=Basic&location=application"
    b.AddItem("A > X > Y > ~Z", icon = "cmd/sc_cut.png", script = s)
    b.AddItem("B > B1", script = s)
    b.AddItem("B > ---")
    b.AddItem("B > B2", script = s)
    b.AddItem("B > B3 > B4", script = s)
    b.AddItem("B > B3 > B5", tooltip = "This the tooltip", script = s)
    b.AddRadioButton("B > B61", status = True, script = s)
    b.AddRadioButton("B > B62", name = "This is B42", script = s)
    b.AddRadioButton("B > B63", script = s)
    id = b.AddRadioButton("B > B64", status = True, script = s)
    b.AddCheckBox("C > C71", script = s)
    b.AddCheckBox("C > C72", name = "This is B42", script = s)
    b.AddCheckBox("C > C73", script = s)
    id = b.AddCheckBox("C > C74", status = True, script = s)
    id = b.AddItem("C > C75", script = s)
    b.AddItem("C > About", command = "About")
    b.AddItem("Script", name = "xxx", script = s)
    b.Dispose()


def OpenOrdersMenu():
    a = CreateScriptService('Database', '/home/jean-pierre/Documents/ScriptForge/Test/QA Files/FB NorthWind.odb')
    b = a.OpenTable('Orders')
    b.Activate()
    c = b.CreateMenu('More')
    c.AddItem('Details',
              script = 'vnd.sun.star.script:QA._Datasheet.OpenOrderDetails?language=Basic&location=application')
    c.AddItem('Remove menu',
              script = 'vnd.sun.star.script:QA._Datasheet.RemoveMenu?language=Basic&location=application')
    c.AddItem('Xray', script = 'vnd.sun.star.script:QA._Datasheet.XrayCurrent?language=Basic&location=application')
    c.AddItem('About', command = 'About')
    c.Dispose()


def PopupMenu(poEvent = None):
    #	Called when clicking on PopupPython button in QADialog dialog

    a = CreateScriptService('popupmenu', poEvent, submenuchar = ' > ')
    exc = CreateScriptService('Exception')
    exc.DebugPrint(a.ShortcutCharacter, a.SubmenuCharacter)
    a.AddItem('A > X > Y > ~Z', icon = 'cmd/sc_cut.png')
    a.AddItem('B > B1')
    a.AddItem('B > ---')
    a.AddItem('B > B2')
    a.AddItem('B > B3 > B4')
    a.AddItem('B > B3 > B5', tooltip = 'This the tooltip')
    a.AddRadioButton('B > B61', status = True)
    a.AddRadioButton('B > B62', name = 'This is B62')
    a.AddRadioButton('B > B63')
    a.AddRadioButton('B > B64', status = True)
    a.AddCheckBox('C > C71')
    a.AddCheckBox('C > C72', name = 'This is C72')
    a.AddCheckBox('C > C73')
    a.AddCheckBox('C > C74', status = True)
    exec = a.Execute(False)
    exc.DebugPrint('PopupMenu', exec)

def TestPageManager():
    dialog = CreateScriptService('Dialog', 'GlobalScope', 'QA', 'QAPages')
    dialog.SetPageManager(pilotcontrols = 'PageList,PageCombo,PageRadio1',
                          tabcontrols = 'PageButton1,PageButton2,PageButton3,PageButton4',
                          wizardcontrols = 'BackButton,NextButton',
                          lastpage = 4)
    dialog.execute()
    dialog.Terminate()

def GetBasicAttributes():
    # Called from QA/Unit_Tests/SF_ExportHelpTree to get the list of pseudo-basic methods
    attributeslist = ''
    bas = CreateScriptService('Basic')
    basd = dir(bas)
    for bb in basd:
        if bb != bb.upper() and bb[0] != bb[0].lower():
            attributeslist += bb + ',' + 'Method' + ',' + bas.servicename + '/'
    return attributeslist

def TestRemoveDuplicates():
    nbRows = 90000
    nbKeys = 2

    ui = CreateScriptService("UI")

    a = ui.OpenDocument("/home/jean-pierre/Documents/ScriptForge/Test/Tests/Test Duplicates.ods")
    b = a.Offset("$Data.$B$2:$K$2", 0, 0, nbRows)
    if 'Work' in a.Sheets:
        a.RemoveSheet('Work')
    a.InsertSheet('Work', beforesheet = 'Stats')
    work = a.CopyToCell(b, '$Work.' + a.Printf('%C1%R1', b))
    keys = [*range(1, nbKeys + 1)]

    t = CreateScriptService('Timer', True)
    a.Echo(False, hourglass = True)
    h = a.RemoveDuplicates(work, keys)
    t.Terminate()
    bas = CreateScriptService('Basic')
    a.Echo()
    bas.MsgBox('Size ' + str(nbRows) + '\nKeys ' + str(nbKeys) + '\nDuration ' + str(t.TotalDuration) \
               + '\nRows ' + str(a.Height(h)))

    a.CloseDocument(False)


def DuplicateCheckBox():
    from scriptforge import CreateScriptService
    basic = CreateScriptService('basic')

    #     Create the dialog
    a = CreateScriptService('NewDialog', 'a_Dialog', (250, 100, 200, 250))
    a.Caption = 'Select one or more items'

    #     Extract the data
    db = CreateScriptService('database', '', 'Bibliography')
    b = db.GetRows('SELECT DISTINCT [Author] FROM [biblio] WHERE [Author] IS NOT NULL ORDER BY [Author]')
    db.CloseDatabase()

    #     Build the 1st item
    x, y = 20, 20
    c = a.CreateCheckBox('Author_0', place = (x, y, a.Width - 2 * x, 10), multiline = False)
    c.Caption = b[0][0]
    c.TripleState = False
    c.Value = False

    #     Build all items
    for i in range(1, len(b)):
        y = y + c.Height + 10
        d = a.CloneControl('Author_0', 'Author_' + str(i), x, y)
        d.Caption = b[i][0]

    a.Resize(height = y + 15)
    a.Execute()

    #     Display result
    s = 'Selected:' + '\n'
    for i in range(0, len(b)):
        d = a.Controls('Author_' + str(i))
        if d.Value:
            s = s + '\n' + d.Caption

    basic.MsgBox(s)
    a.Terminate()
    # exc.Console()

    return None

g_exportedScripts = (main, ControlEvent, TestMenu, PopupMenu, OpenDialog, CloseDialog, TestPageManager,
                     OpenOrdersMenu, TestRemoveDuplicates, DuplicateCheckBox)

if __name__ == "__main__":
    main()
