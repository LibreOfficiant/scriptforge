The **QA** folder contains the Basic and Python source code used for testing the ScriptForge piece of software. It contains additionally a few odf test files.

All the files have to be installed manually in a LibreOffice installation before being run as a (long) sequence of non-regression tests.
